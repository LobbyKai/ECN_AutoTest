#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time : 2022/4/24 15:56
# @Author : fengkai
# @File : base_api.py
import hashlib
import requests
import time
from common.logger_handler import logger
from common.file_handler import FileTool


class BaseApi:
    def  __init__(self, env_params):
        self.config_data = FileTool.read_yaml("config")
        self.env_params = env_params
        self.login_env_params = "env"

    def send(self, method, url, token=None, **kwargs):
        '''
        发起请求，使用哪个工具去发起可被替换
        :return:
        '''
        # 获取header头
        self.get_header()
        env_data = self.config_data.get(self.env_params)
        login_env_data = self.config_data.get(self.login_env_params)
        # 判断配置文件是否存在环境信息
        if env_data:
            if "host" not in env_data or "port" not in env_data:
                assert "配置信息不存在"
        else:
            assert "配置信息不存在"

        # 拼接请求的url
        BaseUrl = f"http://{env_data['host']}:{env_data['port']}"
        LoginUrl = f"http://{login_env_data['host']}:{login_env_data['port']}"
        if url.endswith('login'):
            url = LoginUrl + url
        else:
            url = BaseUrl + url
        logger.info(f"接口请求操作，请求的url为：{url}")
        # 判断是否为登录接口，若不是，判断token是否存在并使用
        if token and not url.endswith('login'):
            self.header["token"] = token
        # 判断是否为登录接口，若是，替换type类型
        elif url.endswith('login'):
            self.header["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8"
        elif url.endswith('patientIndex') or url.endswith('supplierInfo') or \
                url.endswith("getDefaultAddress") or url.endswith("distance") or \
                url.endswith("getMyMedicalList") or url.endswith("getMedicalDetail") or \
                url.endswith("getFamilyList") or url.endswith("getIdcard") or \
                url.endswith("get"):
            self.header["Content-Type"] = "application/x-www-form-urlencoded"
        else:
            assert "登录信息不正确"
        r = requests.request(method=method, url=url, headers=self.header, **kwargs)
        return r

    def sendSpecial(self, method, url, **kwargs):
        env_data = self.config_data.get(self.env_params)
        BaseUrl = f"http://{env_data['host']}:{env_data['port']}"
        url = BaseUrl + url
        # 判断配置文件是否存在环境信息
        if env_data:
            if "host" not in env_data or "port" not in env_data:
                assert "配置信息不存在"
        else:
            assert "配置信息不存在"
        r = requests.request(method=method, url=url, **kwargs)
        return r

    def get_header(self):
        env_app = self.config_data.get("app")
        appkey = None
        curtime = None
        checksum = None
        nonce = None
        if env_app:
            if "appkey" in env_app and env_app["appkey"] and "appsecret" in env_app and env_app["appsecret"]:
                # 获取appkey值
                appkey = env_app["appkey"]
                # 获取appsecret值
                appsecret = env_app["appsecret"]
                # 计算curtime值
                curtime = str(int((time.time() * 1000)))
                # 计算nonce值
                nonce = hashlib.new('sha512', str(time.time()).encode("utf-8")).hexdigest()
                # 计算checksum值
                re = appsecret + nonce + curtime
                checksum = hashlib.sha1(re.encode("utf-8")).hexdigest()
            else:
                assert "配置信息不存在"
        else:
            assert "配置信息不存在"
        # 定义header内容
        self.header = {
            "Content-Type": "application/json;charset=utf-8",
            "AppKey": appkey,
            "CurTime": curtime,
            "CheckSum": checksum,
            "Nonce": nonce
        }
