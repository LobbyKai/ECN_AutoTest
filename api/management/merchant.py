#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/17 14:48
# @Author : fengkai
# @File : merchant.py
from api.work_pharmacy import Work
from common.logger_handler import logger
from common.file_handler import FileTool

class Merchant(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def getBasicList(self):
        '''
        新咨询列表
        '''
        # 查看基础版审核的列表
        data = {
            "pageIndex": 1,
            "pageSize": 10
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/supplierReview/getBasicsSupplierList"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result
