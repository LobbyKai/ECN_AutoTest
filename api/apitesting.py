#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/4/27 1:04
# @Author : fengkai
# @File : apitesting.py

from common.file_handler import FileTool
from api.work_pharmacy import Work

class base(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("login_res")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def patientIndex(self):
        '''
        新咨询列表

        '''
        # 查看新咨询列表的请求参数
        agentid = self.config_path["responseBody"]["orgId"]
        data = {
            "coordinate": "108.94878,34.22259",
            "patientId": "211008943"
        }
        # 查看新咨询列表的请求参数

        url = "/miniHome/patientIndex"
        # 调用base_api封装的send,发起请求
        result = self.send("POST", url, token=self.token, params=data)
        consultid = result.json()["responseBody"]["pharmacy"][0]
        print(consultid)
        return consultid


if __name__ == '__main__':
    d = base("env1")
    # d.ConsultList()
    # d.ConsultApply()
    d.patientIndex()