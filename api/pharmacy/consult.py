#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time : 2022/4/24 21:31
# @Author : fengkai
# @File : consult.py
from api.work_pharmacy import Work
from common.logger_handler import logger
from common.file_handler import FileTool


class Consult(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def ConsultList_new(self, type):
        """
        :param type: 咨询类型 图文2 电话3 续方8
        :param lookupId: 药房创建ID
        :param pharmacyId: 药房ID
        :return:
        """
        # 新咨询列表的请求参数
        lookupId = self.createId
        pharmacy_id = self.pharmacyId
        data = {
            "lookupId": lookupId,
            "pageIndex": 1,
            "pageSize": 10,
            "pharmacyId": pharmacy_id,
            "status": type,
            "signId": None
        }
        # 新咨询列表url
        url = "/pharmacy/consult/getConsultListByParam"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"][0]["id"]
        return result

    def ConsultList_ing(self):
        """
        :param type: 咨询类型 图文2 电话3 续方8
        :param lookupId: 药房创建ID
        :param pharmacyId: 药房ID
        :return:
        """
        # 沟通中列表的请求参数
        type = self.config_path["Consult"]["consultState"]["status2"]
        data = {
            "lookupId": None,
            "pageIndex": 1,
            "pageSize": 10,
            "pharmacyId": None,
            "status": type,
            "signId": None
        }
        # 沟通中列表url
        url = "/pharmacy/consult/getConsultListByParam"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result

    def ConsultList_over(self):
        """
        :param type: 咨询类型 图文2 电话3 续方8
        :param lookupId: 药房创建ID
        :param pharmacyId: 药房ID
        :return:
        """
        # 已结束列表的请求参数
        type = self.config_path["Consult"]["consultState"]["status3"]
        data = {
            "lookupId": None,
            "pageIndex": 1,
            "pageSize": 10,
            "pharmacyId": None,
            "status": type,
            "signId": None
        }
        # 已结束列表url
        url = "/pharmacy/consult/getConsultListByParam"
        # 调用base_api封装的send,发起请求
        result = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        return result

    def ConsultList_right(self):
        data = {}
        # 右树信息列表url
        url = "/pharmacy/consult/getConsultData"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result

    def Consult_occupy(self, type):
        """
        :param type: 接单类型
        :return:
        """
        # 读取患者信息
        user_name = self.config_path["patientInfo"]["name"]
        uesr_id =  self.config_path["patientInfo"]["id"]
        # 药房基本信息
        pharmacy_id = self.pharmacyId
        short_name = self.shortname
        create_id =  self.createId
        login_name = self.createName
        # 咨询单号
        id = self.ConsultList_new("2")
        # 接单的请求参数
        data = {
            "createBy": user_name,
            "createId": uesr_id,
            "id": id,
            "pharmacyId": pharmacy_id,
            "pharmacyName": short_name,
            "signId": create_id,
            "signName": login_name,
            "type": type
        }
        # 接单url
        url = "/pharmacy/consult/occupyConsult"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]["id"]
        return result

    def ConsultFinish(self, id, type):
        """
        :param id: 咨询单号
        :param type: 关单类型
        :return:
        """
        # 结束会话请求参数
        data = {
            "id": id,
            "type": type
        }
        # 结束会话url
        url = "/pharmacy/consult/finishConsult"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]["id"]
        return result

    def GetSelllist(self, productName):
        # 药品信息
        agentid = self.config_path["responseBody"]["orgId"]
        orgid = self.yaml_path["medicine"]["orgid"]
        # 获取可开具药品信息的请求参数
        data = {
            "agentId": agentid,
            "bizType": None,
            "orgId": orgid,
            "pageIndex": 1,
            "pageSize": 10,
            "productName": productName
        }
        # 可开具药品url
        url = "/pharmacy/consult/getMyTypeSellList"
        # 调用base_api封装的send,发起请求
        result = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        consultid = result.json()["responseBody"][0]
        return consultid

    def SaveandSend_Order(self):
        """
        :return:
        """
        druginfo = []
        druginfo.append(self.config_path["drugInfo"]["autoDrugInfo"])
        user_name = self.userName
        pharmacy_id = self.createId
        id = self.ConsultList_ing()["responseBody"][0]["id"]
        type = self.config_path["orderMsg"]["sourceType"]["Graphic"]
        # 保存并发送订单的请求参数
        data = {
            "createBy": user_name,
            "createId": pharmacy_id,
            "sourceId": id,
            "sourceType": type,
            "infoList": druginfo
        }
        # 保存并发送订单url
        url = "/pharmacy/consult/saveTemporaryOrderAndSend"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseMsg"]
        return result