#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/6 15:58
# @Author : fengkai
# @File : pay.py
from common.file_handler import FileTool
from api.work_pharmacy import Work
from common.logger_handler import logger


class Pay(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def getOutorder(self):
        '''
        获取订单信息
        '''
        # 获取病更新sql语句
        pharmcy_id = self.pharmacyId
        sqlStatement = self.config_path["mysqlInfo"]["sqlStatement"]["sys_sub_order"]
        sqlStatement_new = sqlStatement.replace("pharmcayid", pharmcy_id)
        sqlStatement1 = self.config_path["mysqlInfo"]["sqlStatement"]["out_trade_no"]
        # 连接数据库查找子订单编号
        self.sys_sub_order = FileTool.get_mysql(sqlStatement_new, 1)
        # 连接数据库查找支付订单号
        out_trade_no = sqlStatement1.replace("sys_sub_order_number", self.sys_sub_order)
        self.sub_order  = FileTool.get_mysql(out_trade_no, 1)
        self.sub_order1 = FileTool.get_mysql(out_trade_no, 2)
        #判断支付表中和子订单表中给的数据相同
        if self.sys_sub_order == self.sub_order:
            self.suboutaroder = self.sub_order1
        url = "/orders/test?outNo=" + self.suboutaroder
        res = self.send("POST", url)
        logger.info(f"接口请求成功")
        result = res.json()["responseMsg"].split("_")[1]
        if result == self.suboutaroder:
            assert "调取银联接口失败，订单支付失败"
