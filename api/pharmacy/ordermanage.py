#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/5 15:04
# @Author : fengkai
# @File : ordermanage.py
from api.work_pharmacy import Work
from common.logger_handler import logger
from common.file_handler import FileTool

class Order(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def supOrderList(self):
        '''
        订单列表
        '''
        # 获取订单列表
        pharmacy_id = self.pharmacyId
        rolelist = self.rolelist
        data = {
            "distributionType": None,
            "headSupplierId": pharmacy_id,
            "pageIndex": 1,
            "pageSize": 10,
            "roleList": [rolelist]
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/order/supOrderList"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def getOrderDetail(self,OrderNo):
        '''
        获取订单详情
        '''
        # 获取订单详情
        params = {
            "subOrderNo": OrderNo,
            "isSupplier": "1"
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/order/detail"
        # 调用base_api封装的send,发起请求
        res = self.send("GET", url, token=self.token, params=params)
        logger.info(f"接口请求成功")
        result = res.json()
        return result

    def printSourceCode(self, OrderNo):
        '''
        打印溯源码
        '''
        # 获取溯源码信息
        pharmacy_id = self.pharmacyId
        self.sourceInfo = self.config_path["drugInfo"]["sourceInfo"]
        biz_id = self.config_path["drugInfo"]["autoDrugInfo"]["productId"]
        data = {
            "num": "1",
            "bizId": biz_id,
            "orderNo": OrderNo,
            "orgId": pharmacy_id,
            "sourceCodeList": [self.sourceInfo]
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/order/printSourceCode"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"][0]["sourceCode"]
        return result

    def financeOrderList(self):
        '''
        获取财务对账订单列表
        '''
        # 获取订单列表
        pharmacy_id = self.pharmacyId
        rolelist = self.rolelist
        data = {
            "pageIndex": 1,
            "pageSize": 10,
            "roleList": [rolelist],
            "headSupplierId": pharmacy_id
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/order/accountSupOrderList"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def orderReconcile(self):
        '''
        获取财务对账订单列表
        '''
        # 获取订单列表
        ord_num = self.financeOrderList()[0]["subOrderNo"]
        data = {
            "isReconcile": "1",
            "subOrderNoList": [ord_num]
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/order/supOrderReconcile"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseMsg"]
        return result

    def getUnbilledOrder(self):
        '''
        获取财务对账订单列表
        '''
        # 获取订单列表
        pharmacy_id = self.pharmacyId
        data = {
            "pageIndex": 1,
            "pageSize": 10,
            "billingStatus": "1",
            "supplierId": pharmacy_id
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/order/queryUnbilledOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result