#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/16 19:57
# @Author : fengkai
# @File : register.py
import uuid
from requests_toolbelt.multipart.encoder import MultipartEncoder
from api.work_pharmacy import Work
from common.file_handler import FileTool
from common.logger_handler import logger
from common.tool_handler import Tool
from common.db_handler import DBTool
from api.interface_3rd import Interface3rd


class Register(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def upload_image(self):
        # 获取随机数
        ran_str = uuid.uuid4().hex
        # 获取图片绝对地址和图片名称
        rootpath = self.config_path["autoEnvInfo"]["imagePath"]
        filetype = ".jpeg"
        self.image_path = Tool.getFilePathList(rootpath, filetype)[0]
        self.image_name = self.image_path.split("\\")[-1]
        # 上传图片的url
        url = "/fastdfsServer/fileUpload"
        # 构造请求头
        fields = open(self.image_path, "rb")
        multipart_encoder = MultipartEncoder(
            fields = {
                'file': (self.image_name, fields, "image/jpeg")
                # file为路径
            },
            boundary = "----" + "WebKitFormBoundary" + ran_str
        )
        headers = {
            "Content-Type": multipart_encoder.content_type
        }
        res = self.sendSpecial("POST", url, headers=headers, data=multipart_encoder)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def getAddressInfo(self, add):
        # 地图信息url
        url = "/dict/area/getAreaDictPayByAddress"
        data = {
            "address": add
        }
        res = self.send("GET", url, params=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def sendValidateCode(self, number):
        #手机验证码url
        url = "/pharmacy/supplierReg/getValidateCode"
        data = {
            "phoneNum": number
        }
        res = self.send("GET", url, params=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def getVercode(self, number):
        sql = self.config_path["mysqlInfo"]["sqlStatement"]["var_code"]
        # 发送手机验证码
        msg = self.sendValidateCode(number)
        assert msg == "发送成功"
        # 数据库查找手机验证码
        self.code = DBTool().selecttest(sql)[0]["code"]
        return self.code

    def basicSupplier(self):
        """
        :param  supplierType:   供应商类型：0 器械 1 药品 2 餐厅 3 超市
        :param  businessLicense:    营业执照照片
        :param  certificatesHolderType: 证件持有人类型 0 企业法人 1 经办人
        :param  businessLicenseLimit:   有效期限 | 长期
        :param  subjectType:    主体类型 0 个体工商户 1企业
        :param  merchantName:   供应商全称
        :param  customerPhone:  客服电话
        :param  merchantShortName:  供应商简称
        :param  businessLicenseRegNo:   营业执照注册编号
        :param  businessLicenseLimit:
        :param  regAddress: 注册地址
        :param  businessScope:  经营范围
        :param  storeProvince:  门店 省
        :param  storeCity:  门店 市
        :param  storeCounty:    门店 区县
        :param  storeRoad:  门店街道
        :param  storeHouseNo:   门店门牌号
        :param  storeAddress:   门店经营地址
        :param  storeFace:  门头照片
        :param  storeEnvironment:   收银台照片
        :param  contactName:    联系人姓名
        :param  password:   密码
        :param  contactPhone:   登录账号/联系人手机号
        :param  verPassword:    确认密码
        :param  vercode:    注册验证码
        :param  longitude:  经度
        :param  latitude:   纬度
        :param  operatorId: 操作人编号
        :param  operatorName:   操作人名称
        :return
        """
        # 营业执照信息
        image1 = self.upload_image()
        credit_code = Tool.create_num_18()
        self.customer_phone = Tool.create_photo_number()
        self.full_name = "auto_" + Tool.create_random_str(30)
        self.short_name = "auto_" + Tool.create_random_str(20)
        self.reg_address =  "autotest" + Tool.create_random_str(30)
        self.bus_scope = "autotest" + Tool.create_random_str(30)
        # 药房照片
        image2 = self.upload_image()
        image3 = self.upload_image()
        # 药房地址信息
        pharmacy_address = self.config_path["companyInfo"]["address"]
        store_province = self.getAddressInfo(pharmacy_address)["storeProvince"]
        store_city = self.getAddressInfo(pharmacy_address)["storeCity"]
        store_county = self.getAddressInfo(pharmacy_address)["storeCounty"]
        self.longitude = float(Interface3rd().AmapInfo()["geocodes"][0]["location"].split(",")[0])
        self.latitude = float(Interface3rd().AmapInfo()["geocodes"][0]["location"].split(",")[1])
        # 联系人信息
        self.contact_phone = Tool.create_photo_number()
        self.user_name = "auto" + Tool.create_random_str(5)
        var_code = self.getVercode(self.contact_phone)
        passwd = self.yaml_path["login"]["passwd"]
        ver_passwd = self.config_path["mysqlInfo"]["envInfo"]["passwd"]
        # 获取基础版注册url
        url = "/pharmacy/supplierReg/regSupplier"
        data = {
            "supplierType": "1",
            "businessLicense": image1,
            "certificatesHolderType": "0",
            "subjectType": "1",
            "merchantName": self.full_name,
            "customerPhone": self.customer_phone,
            "merchantShortName": self.short_name,
            "businessLicenseRegNo": credit_code,
            "businessLicenseLimit": "|",
            "regAddress": self.reg_address,
            "businessScope": self.bus_scope,
            "storeProvince": store_province,
            "storeCity": store_city,
            "storeCounty": store_county,
            "storeRoad": "",
            "storeHouseNo": "",
            "storeAddress": pharmacy_address,
            "storeFace": image2,
            "storeEnvironment": image3,
            "contactName": self.user_name,
            "password": passwd,
            "contactPhone": self.contact_phone,
            "verPassword": ver_passwd,
            "vercode": var_code,
            "longitude": self.longitude,
            "latitude": self.latitude,
            "operatorId": "",
            "operatorName": self.user_name
        }
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result