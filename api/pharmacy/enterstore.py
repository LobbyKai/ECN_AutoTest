#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/13 9:48
# @Author : fengkai
# @File : enterstore.py
import datetime
from api.work_pharmacy import Work
from common.file_handler import FileTool
from common.db_handler import DBTool
from common.logger_handler import logger


class EnterStore(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def updatetime(self):
        """
        更新扫码进店患者的创建时间
        """
        # 获取当前时间
        now = str(datetime.datetime.now()).split('.')[0]
        patient_id = self.config_path["patientInfo"]["id"]
        # 获取并更新sql语句
        sql1 = self.config_path["mysqlInfo"]["sqlStatement"]["change_time_now"]
        sql2 = sql1.replace("userID", patient_id)
        sql = sql2.replace("today", now)
        # 连接数据库查找子订单编号
        data = DBTool().update(sql)
        return data

    def getTodayList(self):
        """
        :param pharmacyId:  药房注册ID
        :return
        """
        #查询当天的扫码患者的请求参数
        pharmacy_id = self.pharmacyId
        data = {
            "pageIndex": 1,
            "pageSize": 10,
            "pharmacyId": pharmacy_id
        }
        # 查询当天的扫码患者url
        url = "/pharmacy/supplier/getTodayScanRecordBy"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result

    def sendOTCorder(self):
        """
        :param  sourceType: 类型 1用药  2电话 3续方 5扫码
        :return
        """
        # 更新数据库患者时间
        druginfo = []
        druginfo.append(self.config_path["drugInfo"]["autoDrugInfo"])
        # 获取sourceID
        self.source_id = self.getTodayList()["responseBody"][0]["id"]
        user_name = self.config_path["patientInfo"]["name"]
        user_id = self.config_path["patientInfo"]["id"]
        # 向扫码的患者推送OTC订单的请求参数
        data = {
            "createBy": user_name,
            "createId": user_id,
            "sourceId": self.source_id,
            "sourceType": "5",
            "infoList": druginfo
        }
        # 向扫码的患者推送OTC订单url
        url = "/pharmacy/consult/sendScanOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result