#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/4/27 1:04
# @Author : fengkai
# @File : apitesting.py
from common.file_handler import FileTool
from common.logger_handler import logger
from api.work_pharmacy import Work


class H5page(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def HomePageInfo(self):
        '''
        获取药房信息
        '''
        # 参数信息
        pay_type = self.pharmacyId
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "createId": user_id,
            "pharmacyId": pay_type
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/pharmacyHomePage"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        result = res.json()["responseBody"]
        return result

    def ConsultApply(self):
        '''
        药房H5申请图文咨询
        环境信息使用env1
        '''
        # 查看新咨询列表的请求参数
        fullName = self.HomePageInfo()["supplierInfo"]["merchantName"]
        phone_num = self.config_path["patientInfo"]["phoneNum"]
        pharmacy_id = self.pharmacyId
        patient_name = self.config_path["patientInfo"]["name"]
        user_id = self.config_path["patientInfo"]["id"]
        apply_msg = self.config_path["Consult"]["consultMsg"]["message1"]
        data = {
            "createBy": patient_name,
            "createId": user_id,
            "details": apply_msg,
            "patientPhone": phone_num,
            "pharmacyId": pharmacy_id,
            "pharmacyName": fullName
        }
        # 查看新咨询列表的请求参数
        url = "/pharmacy/patient/applyChatConsult"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        print(res)
        logger.info(f"接口请求成功")
        result1 = res.json()["responseBody"]["id"]
        result2 = res.json()["responseBody"]["createBy"]
        result3 = res.json()["responseBody"]["createId"]
        return result1, result2, result3

    def PatientIndex(self):
        '''
        获取患者信息
        '''
        # 参数信息
        user_id = self.config_path["patientInfo"]["id"]
        coordinate = self.config_path["pharmacyInfo"]["coordinate"]
        data = {
            "coordinate": coordinate,
            "patientId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/miniHome/patientIndex"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]["pharmacy"][0]
        return result

    def queryOrder(self):
        '''
        获取订单确认信息1
        '''
        # 查看新咨询列表的请求参数
        sqlStatement = self.config_path["mysqlInfo"]["sqlStatement"]["sys_sub_order"]
        self.subOder = FileTool.get_mysql(sqlStatement, 1)
        patientid = self.config_path["patientInfo"]["id"]
        data = {
            "orderSource": None,
            "subOrderNo": self.subOder,
            "userId": patientid
        }
        # 查看新咨询列表的请求参数
        url = "/patientOrders/queryOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result= res.json()["responseBody"][0]
        return result

    def supplierInfo(self):
        '''
        获取订单确认信息2
        '''
        # 获取药房id
        supplierId = self.config_path["pharmacyInfo"]["orgId"]
        data = {
            "supplierId": supplierId
        }
        # 查看新咨询列表的请求参数
        url = "/product/supplierInfo"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def getDefaultAddress(self):
        '''
        获取订单信息
        '''
        # 获取患者ID
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "userId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/user/deliveryAddress/getDefaultAddress"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def getDistance(self):
        '''
        药房和住址间的距离
        '''
        data = {
            "origins": "108.87241,34.185709",
            "destination": "108.94878,34.22259"
        }
        # 查看新咨询列表的请求参数
        url = "/user/map/distance"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def confirmInfo(self, orderSource, Invoice):
        '''
        药房订单预下单
        '''
        dict_full = {}
        # 获取订单确认信息1
        subOrderList = self.queryOrder()
        subOrderList["selectInvoice"] = Invoice
        # 获取订单确认信息2
        supplierInfo = self.supplierInfo()
        # 获取地址信息
        addressInfo = self.getDefaultAddress()
        # 获取配送信息
        distanceInfo = self.getDistance()
        # 更新子订单列表字段
        r1 = {"supplierInfo": supplierInfo}
        r2 = {"HomeAddressInfo": addressInfo}
        r3 = {"supplierDistance": distanceInfo}
        dict_full.update(subOrderList)
        dict_full.update(r1)
        dict_full.update(r2)
        dict_full.update(r3)
        #获取入参
        openid = self.config_path["patientInfo"]["openid"]
        order_type = self.config_path["orderMsg"]["oderType"]["consultOrder"]
        user_id = self.config_path["patientInfo"]["id"]
        pay_type = self.config_path["patientInfo"]["payType"]
        data = {
            "openid": openid,
            "orderSource": orderSource,
            "orderType": order_type,
            "payType": pay_type,
            "subOrderList": [dict_full],
            "userId": user_id
        }
        # 标注URL
        url = "/patientOrders/confirmInfo"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result

    def confirmOrder(self, orderSource, Invoice):
        '''
        药房订单下单
        '''
        dict_full = {}
        # 获取订单确认信息1
        subOrderList = self.queryOrder()
        subOrderList["selectInvoice"] = Invoice
        # 获取订单确认信息2
        supplierInfo = self.supplierInfo()
        # 获取地址信息
        addressInfo = self.getDefaultAddress()
        # 获取配送信息
        distanceInfo = self.getDistance()
        # 更新子订单列表字段
        r1 = {"supplierInfo": supplierInfo}
        r2 = {"HomeAddressInfo": addressInfo}
        r3 = {"supplierDistance": distanceInfo}
        dict_full.update(subOrderList)
        dict_full.update(r1)
        dict_full.update(r2)
        dict_full.update(r3)
        # 获取入参
        openid = self.config_path["patientInfo"]["openid"]
        order_type = self.config_path["orderMsg"]["oderType"]["consultOrder"]
        user_id = self.config_path["patientInfo"]["id"]
        pay_type = self.config_path["patientInfo"]["payType"]
        data = {
            "openid": openid,
            "orderSource": orderSource,
            "orderType": order_type,
            "payType": pay_type,
            "subOrderList": [dict_full],
            "userId": user_id
        }
        # 标注URL
        url = "/patientOrders/confirmOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]
        return result

    def getOutorder(self):
        '''
        获取订单信息
        '''
        # 获取病更新sql语句
        pharmcy_id = self.pharmacyId
        sqlStatement = self.config_path["mysqlInfo"]["sqlStatement"]["sys_sub_order"]
        sqlStatement_new = sqlStatement.replace("pharmcayid", pharmcy_id)
        sqlStatement1 = self.config_path["mysqlInfo"]["sqlStatement"]["out_trade_no"]
        # 连接数据库查找子订单编号
        self.sys_sub_order = FileTool.get_mysql(sqlStatement_new, 1)
        # 连接数据库查找支付订单号
        out_trade_no = sqlStatement1.replace("sys_sub_order_number", self.sys_sub_order)
        self.sub_order  = FileTool.get_mysql(out_trade_no, 1)
        self.sub_order1 = FileTool.get_mysql(out_trade_no, 2)
        #判断支付表中和子订单表中给的数据相同
        if self.sys_sub_order == self.sub_order:
            self.suboutaroder = FileTool.get_mysql(out_trade_no, 2)
        url = "/orders/test?outNo=" + self.suboutaroder
        res = self.send("POST", url)
        result = res.json()["responseMsg"].split("_")[1]
        if result == self.suboutaroder:
            assert "支付订单失败"

    def getNotPayOrder(self):
        '''
        获取未支付订单列表
        '''
        # 获取药房id
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "pageIndex": 1,
            "userId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/patientOrders/getMyNotPayMallOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"][0]["subOrderNo"]
        return result

    def payOrder(self):
        '''
        支付订单
        '''
        # 获取药房id
        order_num = self.getNotPayOrder()
        value = {"subOrderNo": order_num}
        open_id = self.config_path["patientInfo"]["openid"]
        data = {
            "openid": open_id,
            "subOrderList": [value]
        }
        # 查看新咨询列表的请求参数
        url = "/patientOrders/payOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        logger.info(f"接口请求成功")
        result = res.json()["responseBody"]["appId"]
        return result