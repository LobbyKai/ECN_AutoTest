#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 15:29
# @Author : fengkai
# @File : consult.py
from common.file_handler import FileTool
from api.work_pharmacy import Work


class WechatConsult(Work):
    #继承work类初始化
    def __init__(self, env_params):
        self.config_path = FileTool.read_json("config")
        self.yaml_path = FileTool.read_yaml("config")
        super().__init__(env_params)
        Work.__init__(self, env_params)
        self.access_msg()

    def GetDoctorList(self):
        '''
        获取我的医生的列表
        '''
        # 参数信息
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "pageIndex": 1,
            "pageSize": 10,
            "patientId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/miniMedical/getMyMedicalList"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]
        return result

    def GetDoctoDetail(self):
        '''
        获取选择医生的详情
        '''
        # 参数信息
        doctor_id = self.GetDoctorList()[3]["id"]
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "medicalId": doctor_id,
            "patientId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/miniHome/getMedicalDetail"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]
        return result

    def GetFamilyList(self):
        '''
        获取患者列表的信息
        '''
        # 参数信息
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "patientId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/miniPatient/getFamilyList"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]
        return result

    def GetIdCard(self):
        '''
        获取选择患者的详细信息
        '''
        # 参数信息
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "userPatientId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/user/patient/getIdcard"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]
        return result

    def GetGraphicsText(self):
        '''
        获取选择患者的详细信息
        '''
        # 参数信息
        doctor_id = self.GetDoctorList()[3]["id"]
        user_id = self.config_path["patientInfo"]["id"]
        data = {
            "userMedicalId": doctor_id,
            "patientId": user_id
        }
        # 查看新咨询列表的请求参数
        url = "/user/userMedicalText/get"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]
        return result

    def GetPhoneText(self):
        '''
        获取选择患者的详细信息
        '''
        # 参数信息
        doctor_id = self.GetDoctorList()[3]["id"]
        # user_id = self.config_path["patientInfo"]["id"]
        data = {
            "userMedicalId": doctor_id
        }
        # 查看新咨询列表的请求参数
        url = "/user/userMedicalPhone/get"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]
        return result

    def GetVedioText(self):
        '''
        获取选择患者的详细信息
        '''
        # 参数信息
        doctor_id = self.GetDoctorList()[3]["id"]
        # user_id = self.config_path["patientInfo"]["id"]
        data = {
            "userMedicalId": doctor_id
        }
        # 查看新咨询列表的请求参数
        url = "/user/userMedicalVideo/get"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, params=data)
        result = res.json()["responseBody"]
        return result

    def AddGraphicsInfo(self):
        '''
        添加图文咨询的信息
        '''
        # 参数信息
        user_name = self.GetIdCard()["name"]
        user_age = self.GetIdCard()["age"]
        user_sex = self.GetIdCard()["sex"]
        user_id = self.config_path["patientInfo"]["id"]
        patient_icon = self.GetIdCard()["headIcon"]
        user_card_id = self.GetIdCard()["id"]
        price = self.GetGraphicsText()["price"]
        length = self.GetGraphicsText()["consultLength"]
        doctor_name = self.GetDoctoDetail()["doctorName"]
        doctor_id = self.GetDoctorList()[3]["id"]
        doctor_icon = self.config_path["doctorInfo"]["headIcon"]
        ill_describe = self.config_path["Consult"]["consultMsg"]["message_online1"]
        text_type = self.config_path["orderMsg"]["oderType"]["imageTextType"]
        # data数据
        data = {
            "patientName": user_name,
            "patientAge": user_age,
            "patientSex": user_sex,
            "patientId": user_id,
            "patientIdcardId": user_card_id,
            "patientHeadIcon": patient_icon,
            "doctorId": doctor_id,
            "doctorName": doctor_name,
            "doctorHeadIcon": doctor_icon,
            "price": price,
            "consultLength": length,
            "illInfo": None,
            "illDescribe": ill_describe,
            "imageTextType": text_type
        }
        # 查看新咨询列表的请求参数
        url = "/register/imageTextConsult/add"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        result = res.json()["responseBody"]
        return result

    def AddPhoneInfo(self):
        '''
        添加电话问诊的信息
        '''
        # 参数信息
        user_name = self.GetIdCard()["name"]
        user_age = self.GetIdCard()["age"]
        user_sex = self.GetIdCard()["sex"]
        user_id = self.config_path["patientInfo"]["id"]
        patient_icon = self.GetIdCard()["headIcon"]
        patient_phone = self.config_path["patientInfo"]["phoneNum"]
        user_card_id = self.GetIdCard()["id"]
        price = self.GetPhoneText()["price"]
        length = self.GetPhoneText()["consultLength"]
        doctor_name = self.GetDoctoDetail()["doctorName"]
        doctor_phone = self.GetDoctoDetail()["doctorPhone"]
        doctor_id = self.GetDoctorList()[3]["id"]
        doctor_icon = self.config_path["doctorInfo"]["headIcon"]
        ill_describe = self.config_path["Consult"]["consultMsg"]["message_online2"]
        # data数据
        data = {
            "patientName": user_name,
            "patientAge": user_age,
            "patientSex": user_sex,
            "patientId": user_id,
            "patientPhone": patient_phone,
            "patientIdcardId": user_card_id,
            "patientHeadIcon": patient_icon,
            "doctorId": doctor_id,
            "doctorName": doctor_name,
            "doctorPhone": doctor_phone,
            "doctorHeadIcon": doctor_icon,
            "price": price,
            "consultLength": length,
            "illInfo": None,
            "illDescribe": ill_describe,
        }
        # 查看新咨询列表的请求参数
        url = "/register/phoneConsult/add"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        result = res.json()["responseBody"]
        return result

    def AddVideoInfo(self):
        '''
        添加视频问诊的信息
        '''
        # 参数信息
        user_name = self.GetIdCard()["name"]
        user_age = self.GetIdCard()["age"]
        user_sex = self.GetIdCard()["sex"]
        user_id = self.config_path["patientInfo"]["id"]
        patient_icon = self.GetIdCard()["headIcon"]
        user_card_id = self.GetIdCard()["id"]
        price = self.GetPhoneText()["price"]
        length = self.GetPhoneText()["consultLength"]
        doctor_name = self.GetDoctoDetail()["doctorName"]
        doctor_id = self.GetDoctorList()[3]["id"]
        doctor_icon = self.config_path["doctorInfo"]["headIcon"]
        ill_describe = self.config_path["Consult"]["consultMsg"]["message_online3"]
        # data数据
        data = {
            "patientName": user_name,
            "patientAge": user_age,
            "patientSex": user_sex,
            "patientId": user_id,
            "patientImId": user_id,
            "patientIdcardId": user_card_id,
            "patientHeadIcon": patient_icon,
            "doctorId": doctor_id,
            "doctorImId": doctor_id,
            "doctorName": doctor_name,
            "doctorHeadIcon": doctor_icon,
            "price": price,
            "consultLength": length,
            "illInfo": None,
            "illDescribe": ill_describe,
        }
        # 查看新咨询列表的请求参数
        url = "/register/videoConsult/add"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        result = res.json()["responseBody"]
        return result

    def ConfirmGraphicsOrder(self):
        '''
        提交图文咨询
        '''
        # 参数信息
        user_id = self.config_path["patientInfo"]["id"]
        open_id = self.config_path["patientInfo"]["openid"]
        order_type = self.AddGraphicsInfo()["orderType"]
        order_id = self.AddGraphicsInfo()["orderSource"]
        suborder_list = self.AddGraphicsInfo()["subOrderList"]
        pay_type = self.config_path["patientInfo"]["payType"]
        data = {
            "openid": open_id,
            "orderSource": order_id,
            "orderType": order_type,
            "payType": pay_type,
            "userId": user_id,
            "subOrderList": suborder_list
        }
        # 查看新咨询列表的请求参数
        url = "/patientOrders/confirmOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        result = res.json()["responseMsg"]
        return result

    def ConfirmPhoneOrder(self):
        '''
        提交电话问诊
        '''
        # 参数信息
        user_id = self.config_path["patientInfo"]["id"]
        open_id = self.config_path["patientInfo"]["openid"]
        order_type = self.AddPhoneInfo()["orderType"]
        order_id = self.AddPhoneInfo()["orderSource"]
        suborder_list = self.AddPhoneInfo()["subOrderList"]
        pay_type = self.config_path["patientInfo"]["payType"]
        data = {
            "openid": open_id,
            "orderSource": order_id,
            "orderType": order_type,
            "payType": pay_type,
            "userId": user_id,
            "subOrderList": suborder_list
        }
        # 查看新咨询列表的请求参数
        url = "/patientOrders/confirmOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        result = res.json()["responseMsg"]
        return result

    def ConfirmVideoOrder(self):
        '''
        提交视频问诊
        '''
        # 参数信息
        user_id = self.config_path["patientInfo"]["id"]
        open_id = self.config_path["patientInfo"]["openid"]
        order_type = self.AddVideoInfo()["orderType"]
        order_id = self.AddVideoInfo()["orderSource"]
        suborder_list = self.AddVideoInfo()["subOrderList"]
        pay_type = self.config_path["patientInfo"]["payType"]
        data = {
            "openid": open_id,
            "orderSource": order_id,
            "orderType": order_type,
            "payType": pay_type,
            "userId": user_id,
            "subOrderList": suborder_list
        }
        # 查看新咨询列表的请求参数
        url = "/patientOrders/confirmOrder"
        # 调用base_api封装的send,发起请求
        res = self.send("POST", url, token=self.token, json=data)
        result = res.json()["responseMsg"]
        return result


if __name__ == '__main__':
    d = WechatConsult("env1")
    d.ConfirmGraphicsOrder()