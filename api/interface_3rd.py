#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/17 10:01
# @Author : fengkai
# @File : interface_3rd.py
import requests
from common.file_handler import FileTool
from common.logger_handler import logger


class Interface3rd:
    def __init__(self):
        self.config_path = FileTool.read_json("config")

    def AmapInfo(self):
        # 调用高德接口获取注册信息
        address = self.config_path["companyInfo"]["address"]
        url = "https://restapi.amap.com/v3/geocode/geo"
        data = {
            "key": "ad3d1e9c35138aa32e00c39e74cf7387",
            "address": address
        }
        res = requests.request(method="GET", url=url, params=data)
        logger.info(f"接口请求成功")
        result = res.json()
        return result