#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time : 2022/4/24 20:21
# @Author : fengkai
# @File : work_pharmacy.py
from api.base_api import BaseApi
from common.logger_handler import logger


class Work(BaseApi):
    def __init__(self, env_params):
        #继承BaseApi类的初始化
        BaseApi.__init__(self, env_params)

    def access_msg(self):
        '''
        获取access_token
        :return:
        '''
        url = "/pharmacy/user/login"
        env_login = self.config_data.get("login")
        if env_login:
            if "username" in env_login and env_login["username"] and "passwd" in env_login and env_login["passwd"]:
                self.username = env_login["username"]
                self.passwd = env_login["passwd"]
            else:
                assert "登录信息不存在"
        # 定义请求参数
        params = {
            "loginName": self.username,
            "password": self.passwd
        }
        # 发送get请求
        r = self.send("GET", url=url, params=params)
        # 打印响应
        #logger.info(f"接口请求后，返回的参数为：{r.text}")
        # 获取token
        accesss_token = r.json()["responseBody"]["token"]
        self.token = accesss_token
        # 获取药房ID
        pharmacy_id = r.json()["responseBody"]["orgId"]
        self.pharmacyId = pharmacy_id
        # 获取药房用户号
        user_name = r.json()["responseBody"]["userName"]
        self.userName = user_name
        # 获取药房创建账号
        create_id = r.json()["responseBody"]["id"]
        self.createId = create_id
        create_name = r.json()["responseBody"]["loginName"]
        self.createName = create_name
        # 获取规则名
        roleList = r.json()["responseBody"]["roleList"][0]["id"]
        self.rolelist = roleList
        # 获取简称
        shortName = r.json()["responseBody"]["org"]["orgName"]
        self.shortname = shortName

    def loggout(self):
        """
        登出环境
        :return:
        """
        url = "/pharmacy/user/logout"
        env_login = self.config_data.get("logout")
        if env_login:
            if "userid" in env_login and env_login["userid"]:
                self.userid = env_login["userid"]
            else:
                assert "登出信息不存在"
        # 定义请求参数
        params = {
            "userId": self.userid
        }
        # 发送get请求
        r = self.send("GET", url=url, params=params)
        # 打印响应
        logger.info(f"接口请求后，返回的参数为：{r.text}")
        # 获取登出系统响应体
        res = r.json()["responseMsg"]
        return res