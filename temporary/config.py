# -*- coding: utf-8 -*-
# @Time : 2022/4/24 15:20
# @Author : fengkai
# @File : file_handler.py
import os

#配置文件路径
CONFIG_PATH = os.path.dirname(os.path.abspath(__file__))

#项目路径
ROOT_PATH = os.path.dirname(CONFIG_PATH)

#测试用例路径
CASES_PATH = os.path.join(ROOT_PATH,"test_case")

#测试报告路径
REPORT_PATH = os.path.join(ROOT_PATH,"reports")