#!/usr/bin/env python3
# _*_ coding:utf-8 _*_

import requests
import hashlib
import time
import pymysql
from common.file_handler import FileTool
import json

class TestBaseApi:
    #获取Appkey
    def get_APPKey(self):
        APPKey = "6a9c4160fb813859190f8d307990a4f1"
        return APPKey
    #获取AppSecret
    def get_AppSecret(self):
        AppSecret="e8dd6aae2043"
        return AppSecret
    #获取CurTime
    def get_CurTime(self):
        CurTime = str(int((time.time() * 1000)))
        return CurTime
    #获取Nonce
    def get_Nonce(self):
        Nonce = hashlib.new('sha512',str(time.time()).encode("utf-8")).hexdigest()
        return Nonce
    #获取CheckSum
    def get_CheckSum(self):
        res = TestBaseApi().get_AppSecret() + TestBaseApi().get_Nonce() + TestBaseApi().get_CurTime()
        CheckSum = hashlib.sha1(res.encode("utf-8")).hexdigest()
        return CheckSum
    # 登录药房管理系统
    def test_pharmacy_login(self):
        AppKey = TestBaseApi().get_APPKey()
        CurTime = TestBaseApi().get_CurTime()
        Nonce = TestBaseApi().get_Nonce()
        CheckSum = TestBaseApi().get_CheckSum()
        id = [{"id": "e5413612b66b43cbaf45067b3e1d5517", "subOrderNo": "202204281614404575"}]
        url = "http://192.168.1.199:9090/patientOrders/confirmInfo"
        """
            AppKey	开发者平台分配的appkey
            Nonce	随机数（最大长度128个字符）
            CurTime	当前UTC时间戳，从1970年1月1日0点0 分0 秒开始到现在的秒数(String)
            CheckSum	SHA1(AppSecret + Nonce + CurTime)，三个参数拼接的字符串，进行SHA1哈希计算，转化成16进制字符(String，小写)
        """
        header = {
                "AppKey": AppKey,
                "CurTime": CurTime,
                "Nonce": Nonce,
                "CheckSum": CheckSum
        }
        data = {
            "openid": "onBX74pDcY2D6bKih3y2IWanMBHs",
            "orderSource": "202204271504303266",
            "orderType": "g",
            "payType": "wechatApplet_patient",
            "subOrderList": id,
            "userId": "211008943"
        }
        r = requests.request("POST", url = url, headers = header, json= data)
        res = r.json()
        print(res)
        return res


if __name__ == '__main__':
    print("test")