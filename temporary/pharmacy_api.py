#!/usr/bin/env python
# _*_ coding:utf-8 _*_

import requests

AppKey = TestLogin().test_get_APPKey()
CurTime = TestLogin().test_get_CurTime()
Nonce = TestLogin().test_get_Nonce()
CheckSum = TestLogin().test_get_CheckSum()

class TestConsult:
    ###获取订单列表###
    def getOderList(self, CurTime=CurTime, AppKey=AppKey, CheckSum=CheckSum, Nonce=Nonce):
        url = "http://192.168.1.208:9191/pharmacy/order/supOrderList"
        """
           AppKey	开发者平台分配的appkey
           Nonce	随机数（最大长度128个字符）
           CurTime	当前UTC时间戳，从1970年1月1日0点0 分0 秒开始到现在的秒数(String)
           CheckSum	SHA1(AppSecret + Nonce + CurTime)，三个参数拼接的字符串，进行SHA1哈希计算，转化成16进制字符(String，小写)
        """
        header = {
            "Content-Type": "application/json;charset=utf-8",
            "AppKey": AppKey,
            "CurTime": CurTime,
            "CheckSum": CheckSum,
            "Nonce": Nonce
        }
        data = {
            "distributionType" : None,
            "endTime" : "20220418",
            "headSupplierId" : "3af807e8d44046e1a55a64eb66d63349",
            "pageIndex" : 1,
            "pageSize" : 10,
            "roleList" : ["ac1472328a944fd992d742e11cb598ee"],
            "startTime" : "20220319"
        }
        #获取订单列表
        result = requests.request(method="POST", url=url, headers=header, json=data)
        return result

    #待处理咨询消息
    def geConsultData_New(self,  CurTime=CurTime, AppKey=AppKey, CheckSum=CheckSum, Nonce=Nonce):
        url = "http://192.168.1.208:9191/pharmacy/consult/getConsultListByParam"
        """
                   AppKey	开发者平台分配的appkey
                   Nonce	随机数（最大长度128个字符）
                   CurTime	当前UTC时间戳，从1970年1月1日0点0 分0 秒开始到现在的秒数(String)
                   CheckSum	SHA1(AppSecret + Nonce + CurTime)，三个参数拼接的字符串，进行SHA1哈希计算，转化成16进制字符(String，小写)
        """
        header = {
            "Content-Type": "application/json;charset=utf-8",
            "AppKey" : AppKey,
            "CurTime" : CurTime,
            "CheckSum" : CheckSum,
            "Nonce" : Nonce
        }
        data = {
            "lookupId" : "phar_10000004203",
            "pageIndex" : 1,
            "pageSize" : 10,
            "pharmacyId" : "83360dda77ff444a89f7a45beadc8f87",
            "status" : "2",
            "signId" : None
        }
        result = requests.request(method="POST", url=url, headers=header, json=data)
        return result

    #处理中咨询消息
    def geConsultData_Ing(self,  CurTime=CurTime, AppKey=AppKey, CheckSum=CheckSum, Nonce=Nonce):
        url = "http://192.168.1.208:9191/pharmacy/consult/getConsultListByParam"
        """
                   AppKey	开发者平台分配的appkey
                   Nonce	随机数（最大长度128个字符）
                   CurTime	当前UTC时间戳，从1970年1月1日0点0 分0 秒开始到现在的秒数(String)
                   CheckSum	SHA1(AppSecret + Nonce + CurTime)，三个参数拼接的字符串，进行SHA1哈希计算，转化成16进制字符(String，小写)
        """
        header = {
            "Content-Type": "application/json;charset=utf-8",
            "AppKey" : AppKey,
            "CurTime" : CurTime,
            "CheckSum" : CheckSum,
            "Nonce" : Nonce
        }
        data = {
            "lookupId" : None,
            "pageIndex" : 1,
            "pageSize" : 10,
            "pharmacyId" : None,
            "status" : "2",
            "signId" : None
        }
        result = requests.request(method="POST", url=url, headers=header, json=data)
        return result

    #已结束咨询消息
    def geConsultData_Done(self,  CurTime=CurTime, AppKey=AppKey, CheckSum=CheckSum, Nonce=Nonce):
        url = "http://192.168.1.208:9191/pharmacy/consult/getConsultListByParam"
        """
                   AppKey	开发者平台分配的appkey
                   Nonce	随机数（最大长度128个字符）
                   CurTime	当前UTC时间戳，从1970年1月1日0点0 分0 秒开始到现在的秒数(String)
                   CheckSum	SHA1(AppSecret + Nonce + CurTime)，三个参数拼接的字符串，进行SHA1哈希计算，转化成16进制字符(String，小写)
        """
        header = {
            "Content-Type": "application/json;charset=utf-8",
            "AppKey" : AppKey,
            "CurTime" : CurTime,
            "CheckSum" : CheckSum,
            "Nonce" : Nonce
        }
        data = {
            "lookupId" : None,
            "pageIndex" : 1,
            "pageSize" : 10,
            "pharmacyId" : None,
            "status" : "2",
            "signId" : None
        }
        result = requests.request(method="POST", url=url, headers=header, json=data)
        return result