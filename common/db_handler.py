#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/13 14:32
# @Author : fengkai
# @File : db_handler.py
#导包 pymysql
import pymysql
from common.file_handler import FileTool


# 新建工具类 数据库
class DBTool:
    # 初始化
    def __init__(self):
        self.config = FileTool.read_json("config")["mysqlInfo"]["envInfo"]
        self.mydb = pymysql.connect(**self.config)
        self.cursor = self.mydb.cursor(cursor=pymysql.cursors.DictCursor)

    # 关闭数据库
    def __del__(self):
        self.mydb.close()
        self.cursor.close()

    # 获取表数据
    def selecttest(self, sql):
        self.cursor.execute(sql)
        data = self.cursor.fetchall()
        return data

    # 更新表数据
    def update(self, sql):
        try:
            self.cursor.execute(sql)
            self.mydb.commit()
        except Exception as e:
            self.mydb.rollback()
            print('在更新数据库时发生错误了：{0}'.format(e))

    # 插入表数据
    def insert(self, sql):
        try:
            self.cursor.execute(sql)
            self.mydb.commit()
        except Exception as e:
            self.mydb.rollback()
            print('在插入数据时发生错误了：{0}'.format(e))