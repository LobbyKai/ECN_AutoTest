#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/16 20:25
# @Author : fengkai
# @File : tool_handler.py
import random
import string
import os


class Tool():
    @classmethod
    def getFilePathList(cls, rootpath, filetype):
        # 获取文件的绝对路径
        filepath = []
        # 获取所有文件下的子文件名称
        for root, dirs, files in os.walk(rootpath):
            # 过滤所有空文件
            if files:
                for file in files:
                    path = os.path.join(root, file)
                    # 判断只返回type类型的文件
                    if filetype in path:
                        # filepath.append(str(path.replace("\\", "/")))
                        filepath.append(str(path))
        return filepath

    @classmethod
    def create_num_18(cls):
        # 随机生成18位数字
        res = random.randint(0, 1000000000000000000)
        return str(res)

    @classmethod
    def create_photo_number(cls):
        # 随机生成手机号码
        res = random.randint(0, 100000000)
        result = "137" + str(res)
        return result

    @classmethod
    def create_random_str(cls, number):
        # 随机生成指定长度的字符串
        res = ''.join(random.sample(string.ascii_letters + string.digits, number))
        return res
