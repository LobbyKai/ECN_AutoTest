#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/16 15:15
# @Author : fengkai
# @File : test_smoking_invoiceList_0010.py
import pytest
import allure
from api.pharmacy.consult import Consult
from api.pharmacy.ordermanage import Order
from common.file_handler import FileTool
from common.logger_handler import logger
from api.work_pharmacy import Work
from api.pharmacy.h5page import H5page
from api.pharmacy.pay import Pay

Consult = Consult("env")
H5page = H5page("env1")
Order = Order("env")
Work = Work("env")
Pay = Pay("env2")

@allure.feature("发票管理测试")
class TestPharmacy:
    @allure.story("购药测试")
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")

    @pytest.mark.parametrize(("msg1", "msg2"), [("生成订单成功", "交易成功")])
    def testcase(self, msg1, msg2):
        self.patient_name = self.config_path["patientInfo"]["name"]
        self.patient_id = self.config_path["patientInfo"]["id"]
        # 患者端发起图文咨询
        self.consultId, self.name, self.id = H5page.ConsultApply()
        assert self.name == self.patient_name
        assert self.id == self.patient_id
        logger.info(f"发起图文咨询成功，咨询ID已获取")
        # 登录药房系统并查看新咨询
        self.new_id = Consult.ConsultList_new("2")
        assert self.new_id == self.consultId
        logger.info(f"药房收到咨询单,咨询号是")
        # 接单
        self.occupy_id = Consult.Consult_occupy("1")
        assert self.occupy_id == self.consultId
        # 登录药房系统并查看新咨询
        self.receive_id = Consult.ConsultList_new("3")
        assert self.receive_id == self.consultId
        logger.info(f"接单成功，沟通中存在咨询会话")
        # 选择OTC药品并发送
        self.message = Consult.SaveandSend_Order()
        assert self.message == msg1
        logger.info(f"给患者发送OTC药品订单成功")
        # 登录小程序，获取咨询订单信息
        self.wechat_id = int(self.consultId)
        # 获取子订单号
        self.sub_oder_id = H5page.queryOrder()["subOrderNo"]
        # 进入预支付界面
        self.pre_pay_order = H5page.confirmInfo(self.wechat_id, Invoice="1")["responseBody"]["subOrderList"][0] \
            ["goodsList"][0]["subOrderNo"]
        assert  self.sub_oder_id == self.pre_pay_order
        logger.info(f"预订单支付成功")
        # 进入支付界面，调出二维码
        self.pay_msg = H5page.confirmOrder(self.wechat_id, Invoice="1")
        if self.pay_msg != None:
            assert "订单支付失败"
        logger.info(f"订单支付成功")
        # 获取url字段并调取银联接口
        self.bank_msg = Pay.getOutorder()
        logger.info(f"调取银联接口成功")
        # 登录药房系统查看订单编号
        self.order_num = Order.supOrderList()[0]["subOrderNo"]
        assert self.order_num == self.sub_oder_id
        logger.info(f"订单列表存在订单信息，订单支付完成")
        # 打印溯源码
        self.source_num = Order.printSourceCode(self.order_num)
        if self.source_num != None:
            assert "打印溯源码失败"
        # 查看订单状态是否变更为已出库
        self.order_status = Order.getOrderDetail(self.order_num)["responseBody"]["orderStatus"]
        assert self.order_status == "6"
        logger.info(f"溯源码打印成功，订单状态变更为'已出库'")
        # 查看财务对账订单信息并对账
        self.message = Order.orderReconcile()
        assert self.message == msg2
        logger.info(f"对账成功")
        # 查看订单检查对账状态是否变化
        self.reconcile_status = Order.financeOrderList()[0]["isReconcile"]
        assert self.reconcile_status == 1
        logger.info(f"对账状态变更为'已对账'")
        # 查看发票状态
        self.billed_num = Order.getUnbilledOrder()["responseBody"][0]["subOrderNo"]
        self.invoice_status = Order.getUnbilledOrder()["responseBody"][0]["selectInvoice"]
        assert self.billed_num == self.order_num
        assert self.invoice_status == "1"
        logger.info(f"订单选择开票")

    def teardown_class(self):
        #查看咨询单号并关闭订单
        self.consultId = Consult.ConsultList_new("3")
        self.finish_id = Consult.ConsultFinish(self.consultId, "1")
        assert self.finish_id == self.consultId
        self.over_id = Consult.ConsultList_new("8")
        assert self.over_id == self.finish_id
        #查看已结束咨询会话列表
        logger.info(f"咨询已关闭")
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")

if __name__ == '__main__':
    pytest.main(['-s', '-v', '--html=/reports/report.html'])