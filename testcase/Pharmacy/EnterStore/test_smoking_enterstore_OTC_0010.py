#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/13 9:47
# @Author : fengkai
# @File : test_smoking_enterstore_OTC_0010.py
import allure
import pytest
from api.pharmacy.ordermanage import Order
from api.pharmacy.enterstore import EnterStore
from common.file_handler import FileTool
from common.logger_handler import logger
from api.work_pharmacy import Work
from api.pharmacy.h5page import H5page
from api.pharmacy.pay import Pay

H5page = H5page("env1")
Order = Order("env")
Work = Work("env")
EnterStore = EnterStore("env")
Pay = Pay("env2")

@allure.feature("扫码进店OTC开药测试")
class TestPharmacy:
    @allure.story("购药测试")
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")

    def testcase(self):
        self.patient_name = self.config_path["patientInfo"]["name"]
        self.patient_id = self.config_path["patientInfo"]["id"]
        # 登录数据库修改患者的创建时间
        self.updateTime = EnterStore.updatetime()
        assert "数据库更新失败"
        logger.info(f"数据库更新成功")
        # 查询当天的扫码患者
        self.todayList = EnterStore.getTodayList()
        self.AutoID = self.todayList["responseBody"][0]["patientId"]
        assert self.AutoID == self.patient_id
        logger.info(f"患者信息已成功刷新")
        # 给进店患者发送OTC药品订单
        self.sendOTC = EnterStore.sendOTCorder()
        if self.sendOTC != None:
            assert "订单发送失败"
        logger.info(f"订单发送成功")
        # 进入支付界面，调出二维码
        self.sub_oder_id = H5page.getNotPayOrder()
        self.payOrder = H5page.payOrder()
        if self.payOrder != None:
            assert "订单支付失败"
        logger.info(f"订单支付成功")
        # 获取url字段并调取银联接口
        self.bank_msg = Pay.getOutorder()
        logger.info(f"调取银联接口成功")
        # 登录药房系统查看订单编号
        self.order_num = Order.supOrderList()[0]["subOrderNo"]
        assert self.order_num == self.sub_oder_id
        logger.info(f"订单列表存在订单信息，订单支付完成")

    def teardown_class(self):
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")


if __name__ == '__main__':
    pytest.main(['-s', '-v', '--html=/reports/report.html'])