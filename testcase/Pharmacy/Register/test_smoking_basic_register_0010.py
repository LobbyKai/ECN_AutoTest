#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/16 21:46
# @Author : fengkai
# @File : test_smoking_basic_register_0010.py
import allure
import pytest
from api.management.merchant import Merchant
from common.logger_handler import logger
from api.work_pharmacy import Work
from common.file_handler import FileTool
from api.pharmacy.register import Register

Work = Work("env")
Register = Register("env")
Merchant = Merchant("env1")

@allure.feature("基础版药房注册测试")
class TestPharmacy:
    @allure.story("购药测试")
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")

    def testcase(self):
        # 提交基础药房注册
        self.pharmacy_id = Register.basicSupplier()["responseBody"]
        # 管理端查看新注册药房信息
        self.id = Merchant.getBasicList()["responseBody"][0]["id"]
        assert self.pharmacy_id == self.id
        logger.info(f"基础版药房注册成功")

    def teardown_class(self):
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")


if __name__ == '__main__':
    pytest.main(['-s', '-v', '--html=/reports/report.html'])
