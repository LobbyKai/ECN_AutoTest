#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/6 19:49
# @Author : fengkai
# @File : test_func_consult_0011.py
import allure
import pytest
from api.pharmacy.consult import Consult
from common.file_handler import FileTool
from common.logger_handler import logger
from api.work_pharmacy import Work
from api.pharmacy.h5page import H5page

Consult = Consult("env")
H5page = H5page("env1")
Work = Work("env")

@allure.feature("图文咨询测试")
class TestPharmacy:
    @allure.story("购药测试")
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")

    @pytest.mark.parametrize("msg", ["生成订单成功"])
    def testcase(self, msg):
        self.patient_name = self.config_path["patientInfo"]["name"]
        self.patient_id = self.config_path["patientInfo"]["id"]
        # 患者端发起图文咨询
        self.consultId, self.name, self.id = H5page.ConsultApply()
        assert self.name == self.patient_name
        assert self.id == self.patient_id
        logger.info(f"发起图文咨询成功，咨询ID已获取")
        # 登录药房系统并查看新咨询
        self.new_id = Consult.ConsultList_new("2")
        assert self.new_id == self.consultId
        logger.info(f"药房收到咨询单,咨询号是")
        # 接单
        self.occupy_id = Consult.Consult_occupy("1")
        assert self.occupy_id == self.consultId
        # 在沟通中的咨询列表查看该会话
        self.receive_id = Consult.ConsultList_new("3")
        assert self.receive_id == self.consultId
        logger.info(f"接单成功，沟通中存在咨询会话")

    def teardown_class(self):
        #查看咨询单号并关闭订单
        self.consultId = Consult.ConsultList_new("3")
        self.finish_id = Consult.ConsultFinish(self.consultId, "1")
        assert self.finish_id == self.consultId
        self.over_id = Consult.ConsultList_new("8")
        assert self.over_id == self.finish_id
        #查看已结束咨询会话列表
        logger.info(f"咨询已关闭")
        # 登出环境
        res = Work.loggout()
        assert res == "登出成功"
        logger.info(f"药房系统已登出，环境已恢复")


if __name__ == '__main__':
    pytest.main(['-s', '-v', '--html=/reports/report.html'])