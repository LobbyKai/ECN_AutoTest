#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 13:53
# @Author : fengkai
# @File : base_view.py

from appium.webdriver.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
import time
import os

from app.temporary.driver import BaseDriver


class BaseView:
    def __init__(self, driver: WebDriver = None):
        self.driver = driver
        self.WebDriverWait = driver.implicitly_wait(10)

    def find_element(self, *key):
        """
        查找元素，添加显示等待,每1s查询一次，持续10s，大于10s抛异常
        :param key:
        :return:
        """
        s_time = time.time()
        # imageName = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time())) + '.png'  # 用当前时间戳定义截图名称
        try:
            WebDriverWait(self.driver, 10, 1).until(EC.presence_of_element_located(key))
        except Exception as msg:
            print('获取元素失败{}'.format(msg))
            self.getImage()
        finally:
            element = self.driver.find_element(*key)
            e_time = time.time()
            # print('获取元素成功，用时：{}'.format(e_time - s_time))
            return element

    def find_elements(self, *key):
        """
        查找获取元素list，添加显示等待,每1s查询一次，持续10s，大于10s抛异常
        :param key:
        :return:
        """
        s_time = time.time()
        # imageName = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time())) + '.png'  # 用当前时间戳定义截图名称
        try:
            WebDriverWait(self.driver, 10, 1).until(EC.presence_of_element_located(key))
        except Exception as msg:
            print('获取元素失败{}'.format(msg))
            self.getImage()
        finally:
            elements = self.driver.find_elements(key)
            e_time = time.time()
            # print('获取元素成功，用时：{}'.format(e_time - s_time))
        return elements

    def click_by_id(self, ele_id):
        """
        method explain:根据ID点击
        """
        self.find_element(By.ID, ele_id).click()

    def click_by_class(self, ele_class):
        """
        method explain:class_name
        """
        self.find_element(By.CLASS_NAME, ele_class).click()

    def click_by_xpath(self, xp):
        """
        method explain:根据XPATH点击
        """
        self.find_element(By.XPATH, xp).click()

    def click_by_text(self, ele_text):
        """
        method explain:根据text点击
        """
        self.driver.find_elements_by_android_uiautomator("new UiSelector().text(\"%s\")" % ele_text)[0].click()

    def find_text(self, fd_text):
        """
        查找text
        """
        time.sleep(3)
        return self.driver.find_element_by_android_uiautomator('new UiSelector().text("%s")' % fd_text)

    def input_by_id(self, ele_id, text):
        """
        method explain:根据元素ID，用send_keys方法输入数据
        """
        self.find_element(By.ID, ele_id).send_keys(text)

    def get_text_by_id(self, ele_id):
        """
        method explain:根据元素ID获取text
        """
        return self.find_element(By.ID, ele_id).text

    def get_text_by_xpath(self, ele_id):
        """
        method explain:根据元素Xpath定位获取text
        """
        return self.find_element(By.XPATH, ele_id).text

    def swipe(self, size):
        """
        滑屏
        """
        self.driver.swipe(size["width"] * 0.9, size["height"] * 0.5, size["width"] * 0.1, size["height"] * 0.5)
        time.sleep(2)

    def getImage(self):
        """
        截取图片,并保存在report/images文件夹
        :return: 无
        """
        timestrmap = time.strftime('%Y%m%d_%H%M%S')
        imgPath = os.path.join(get_file_path("report/images"), '%s.png' % str(timestrmap))
        print(imgPath)
        self.driver.save_screenshot(imgPath)
        print('screenshot:', timestrmap, '.png')

    # def get_window_size(self):
    #     """
    #     获取手机屏幕的分辨率
    #     :return:
    #     """
    #     x = self.driver.get_window_size()['width']
    #     y = self.driver.get_window_size()['height']
    #     return x, y

    def swipe_up(self, t=500, n=1):
        """
        向上滑动屏幕
        t=滑动事件时间，毫秒
        n=次数
        """
        l: dict = self.driver.get_window_size()
        x1 = l['width'] * 0.5  # x坐标
        y1 = l['height'] * 0.75  # 起始y坐标
        y2 = l['height'] * 0.25  # 终点y坐标
        for i in range(n):
            self.driver.swipe(x1, y1, x1, y2, t)

    def swipe_down(self, t=500, n=1):
        """
        向下滑动屏幕
        t=滑动事件时间，毫秒
        n=次数
        """
        l = self.driver.get_window_size()
        x1 = l['width'] * 0.5  # x坐标
        y1 = l['height'] * 0.25  # 起始y坐标
        y2 = l['height'] * 0.75  # 终点y坐标
        for i in range(n):
            self.driver.swipe(x1, y1, x1, y2, t)

    def swip_left(self, t=500, n=1):
        """
        向左滑动屏幕
        t=滑动事件时间，毫秒
        n=次数
        """
        l = self.driver.get_window_size()
        x1 = l['width'] * 0.75  # x坐标
        y1 = l['height'] * 0.5  # 起始y坐标
        x2 = l['width'] * 0.25  # 终点y坐标
        for i in range(n):
            self.driver.swipe(x1, y1, x2, y1, t)

    def swip_right(self, t=500, n=1):
        """
        向右滑动屏幕
        t=滑动事件时间，毫秒
        n=次数
        """
        l: dict = self.driver.get_window_size()
        x1 = l['width'] * 0.25  # x坐标
        y1 = l['height'] * 0.5  # 起始y坐标
        x2 = l['width'] * 0.75  # 终点y坐标
        for i in range(n):
            self.driver.swipe(x1, y1, x2, y1, t)