#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 10:45
# @Author : fengkai
# @File : driver.py
from common.file_handler import FileTool
from appium import webdriver

class BaseDriver:
    def __init__(self, env_params):
        self.yaml_path = FileTool.read_yaml("config")

    def get_driver(self, device, automationName="appium", noReset=True) -> object:
        dataDevice = self.yaml_path  # 读取yaml文件
        for i in dataDevice:
            if device == i["deviceDesc"]:
                if automationName != "appium":
                    i["desired_caps"]["automationName"] = automationName
                if not noReset:
                    i["desired_caps"]["noReset"] = False
                desired_caps = i["desired_caps"]
                driver = webdriver.Remote("http://{0}:{1}/wd/hub".format(i["server_url"], i["server_port"]),
                                          desired_caps)
                return driver