#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 17:18
# @Author : fengkai
# @File : drug_search_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.page.medical_records.medicament.drug_edit_page import EditDrugPage


class DrugListPage(BasePage):
    drugname_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/edit']", "咽炎片")
    search_element = (MobileBy.XPATH, "//*[@text='搜索']")
    add_element = (MobileBy.XPATH, "//*[@text='大头儿子']/../../..//*[@resource-id='com.yzn.doctor_hepler:id/add']")

    def input_drugname(self):
        # 搜索药品名
        self.find_and_send(*self.drugname_element)
        return self

    def click_search(self):
        # 点击[搜索]
        self.find_and_click(*self.search_element)
        return self

    def click_add(self):
        # 搜索药品名
        self.input_drugname()
        # 点击[搜索]
        self.click_search()
        # 选择药品，点击[添加]
        self.find_and_click(*self.add_element)
        return EditDrugPage(self.driver)