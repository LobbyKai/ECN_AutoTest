#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 10:12
# @Author : fengkai
# @File : diagnosis_edit_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class DiagnosisEditPage(BasePage):
    diagnosis_element= (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editText' and @text='请输入...']",
                        "Auto_test")
    confirm_element = (MobileBy.XPATH, "//*[@text='保存']")

    def send_info(self):
        # click [诊断信息]
        self.find_and_send(*self.diagnosis_element)
        return self

    def click_confirm(self):
        from app.page.medical_records.medicament.case_info_page import CaseInfoPage
        # 输入信息
        res = self.send_info()
        # 点击[保存]
        self.find_and_click(*self.confirm_element)
        return CaseInfoPage(self.driver)
