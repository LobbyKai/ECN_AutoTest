#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/27 14:04
# @Author : fengkai
# @File : taboo_list_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.base.base_page import BasePage


class TabooListPage(BasePage):
    taboo_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editText']", "Autotest")
    save_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/confirm' and @text='保存']")

    def input_taboo(self):
        # 输入禁忌信息
        self.find_and_send(*self.taboo_element)
        return self

    def click_confirm(self):
        # click [保存]
        from app.page.medical_records.medicament.case_info_page import CaseInfoPage
        self.input_taboo()
        self.find_and_click(*self.save_element)
        return CaseInfoPage(self.driver)

