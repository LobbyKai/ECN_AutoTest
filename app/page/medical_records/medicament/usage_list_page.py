#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 19:49
# @Author : fengkai
# @File : usage_list_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class UsageListPage(BasePage):
    usage_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/text' and @text='口服']")

    def click_usagetype(self):
        from app.page.medical_records.medicament.drug_edit_page import EditDrugPage
        # click [口服]
        self.find_and_click(*self.usage_element)
        return EditDrugPage(self.driver)