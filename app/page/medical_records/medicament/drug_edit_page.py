#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 18:09
# @Author : fengkai
# @File : drug_edit_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.page.medical_records.medicament.frequency_list_page import FrequencyListPage
from app.page.medical_records.medicament.usage_list_page import UsageListPage


class EditDrugPage(BasePage):
    usercount_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/medicineUseCount']", "1")
    usage_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/medicineUsage']")
    frequency_element =(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/medicineFrequency']")
    confirm_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/confirmButton' and @text='确定']")

    def input_usercount(self):
        # 输入用量
        self.find_and_send(*self.usercount_element)
        return self

    def click_usage(self):
        # 点击用法
        self.find_and_click(*self.usage_element)
        return UsageListPage(self.driver)

    def click_frequency(self):
        # 点击用药频率
        self.find_and_click(*self.frequency_element)
        return FrequencyListPage(self.driver)

    def click_confirm(self):
        from app.page.medical_records.medicament.case_info_page import CaseInfoPage
        # 点击确定
        self.find_and_click(*self.confirm_element)
        return CaseInfoPage(self.driver)

