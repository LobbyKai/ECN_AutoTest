#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 10:12
# @Author : fengkai
# @File : case_info_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy

from app.page.medical_records.medicament.allergy_list_page import AllergyListPage
from app.page.medical_records.medicament.diagnosis_edit_page import DiagnosisEditPage
from app.page.medical_records.medicament.drug_search_page import DrugListPage
from app.page.medical_records.medicament.taboo_list_page import TabooListPage


class CaseInfoPage(BasePage):
    diagnosis_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/diagnose']")
    drugadd_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/addMedicineImage']")
    submit_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/submitButton']")
    username_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/patientName']")
    allergy_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/allergy_num']")
    taboo_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/taboo']")

    def click_diagnosis(self):
        # click [临床诊断]
        self.find_and_click(*self.diagnosis_element)
        return DiagnosisEditPage(self.driver)

    def click_allergy(self):
        # click [过敏史]
        self.find_and_click(*self.allergy_element)
        return AllergyListPage(self.driver)

    def click_taboo(self):
        # click [诊断禁忌]
        self.find_and_click(*self.taboo_element)
        return TabooListPage(self.driver)

    def click_drugadd(self):
        # click [添加药品按钮]
        self.find_and_click(*self.drugadd_element)
        return DrugListPage(self.driver)

    def click_submit(self):
        from app.page.messages.session_info_page import SessionInfoPage
        # click [提交]
        self.find_and_click(*self.submit_element)
        return SessionInfoPage(self.driver)

    def get_username(self):
        user_name = self.get_text(*self.username_element)
        return user_name

    def get_result(self):
        # 获取结果
        result = self.getattr(MobileBy.XPATH, "//*[@class='android.widget.Toast']", "text")
        return result



