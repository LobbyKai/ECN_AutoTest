#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 13:44
# @Author : fengkai
# @File : login_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.base.base_page import BasePage
from app.page.main_page import MainPage
from app.page.register.register_main_page import RegisterPage


class LoginPage(BasePage):
    phone_element = (MobileBy.XPATH, "//*[@text='+86']")
    clear_phone_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/clearPhone']")
    edit_phone_element =  (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPhone']", "17792989619")
    passwd_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']")
    clear_passwd_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']")
    edit_passwd_element =  (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']", "989619")
    agree_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/iv_agree_check']")
    login_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/login' and @text='登录'] ")
    verify_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/loginWithCode'"
                                      " and @text='验证码登录'] ")
    code_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editCode']", "123456")
    new_phone_element =  (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPhone']", "15991698205")


    def clear_phone(self):
        # clear [电话号码]
        self.find_and_clear(*self.clear_phone_element)
        return self

    def input_phone(self):
        # 输入 [电话号码]
        self.find_and_send(*self.edit_phone_element)
        return self

    def input_phone_new(self):
        # 输入 [电话号码]
        self.find_and_send(*self.new_phone_element)
        return self

    def clear_passwd(self):
        # clear [密码]
        self.find_and_clear(*self.passwd_element)
        return self

    def input_passwd(self):
        # 输入 [密码]
        self.find_and_send(*self.edit_passwd_element)
        return self

    def click_agree(self):
        # click [同意勾选]
        self.find_and_click(*self.agree_element)
        return self

    def click_login(self):
        self.clear_phone()
        self.input_phone()
        self.clear_passwd()
        self.input_passwd()
        self.click_agree()
        # click [登录]
        self.find_and_click(*self.login_element)
        return MainPage(self.driver)

    def click_verfiy(self):
        # click [验证码登录]
        self.find_and_click(*self.verify_element)
        return self

    def input_code(self):
        # 输入 [验证码]
        self.find_and_send(*self.code_element)
        return self

    def verfiy_login(self):
        self.click_verfiy()
        self.clear_phone()
        self.input_phone()
        self.input_code()
        self.click_agree()
        # click [登录]
        self.find_and_click(*self.login_element)
        return MainPage(self.driver)

    def register_login(self):
        self.click_verfiy()
        self.clear_phone()
        self.input_phone_new()
        self.input_code()
        self.click_agree()
        # click [登录]
        self.find_and_click(*self.login_element)
        return RegisterPage(self.driver)