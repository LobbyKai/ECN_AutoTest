#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 16:17
# @Author : fengkai
# @File : session_info_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.page.medical_records.medicament.case_info_page import CaseInfoPage
from app.page.messages.vedio_chat_page import VedioChatPage


class SessionInfoPage(BasePage):
    prescribe_element = (MobileBy.XPATH, "//*[@text='开具处方']")
    finish_element = (MobileBy.XPATH, "//*[@text='结束咨询']")
    confirm_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/confirm' and @text='确定']")
    vedio_chat_element =  (MobileBy.XPATH, "//*[@text='视频通话']")
    phone_chat_element = (MobileBy.XPATH, "//*[@text='拨打电话']")

    def click_prescribe(self):
        # click [开具处方]
        self.find_and_click(*self.prescribe_element)
        return CaseInfoPage(self.driver)

    def click_phone_chat(self):
        # click [拨打电话]
        self.find_and_click(*self.phone_chat_element)
        return self

    def click_video_chat(self):
        # click [视频通话]
        self.find_and_click(*self.vedio_chat_element)
        return VedioChatPage(self.driver)

    def click_finish(self):
        # click [结束咨询]
        self.find_and_click(*self.finish_element)
        return self

    def click_confirm(self):
        from app.page.messages.message_list_page import MessageListPage
        # click [完成患者咨询-收费]，返回患者列表页
        self.click_finish()
        self.find_and_click(*self.confirm_element)
        return MessageListPage(self.driver)

    def click_over(self):
        from app.page.main_page import MainPage
        # click [完成患者咨询-收费]，返回主页
        self.click_finish()
        self.find_and_click(*self.confirm_element)
        return MainPage(self.driver)

    def get_loading(self):
        self.click_phone_chat()
        # 获取加载信息
        result = self.getattr(MobileBy.XPATH, "//*[@class='android.widget.Toast']", "text")
        return result

    def get_result(self):
        # 获取结果
        result = self.getattr(MobileBy.XPATH, "//*[@class='android.widget.Toast']", "text")
        return result
