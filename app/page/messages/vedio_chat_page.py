#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 9:27
# @Author : fengkai
# @File : vedio_chat_page.py
from appium.webdriver.common.touch_action import TouchAction
from app.base.base_page import BasePage

class VedioChatPage(BasePage):

    def click_hangup(self):
        from app.page.messages.vedio_accept_page import VedioAcceptPage
        # click [挂断]
        TouchAction(self.driver).tap(x=360, y=1120).perform()
        return VedioAcceptPage(self.driver)

    def click_hangup_session(self):
        from app.page.messages.session_info_page import SessionInfoPage
        # click [挂断]
        TouchAction(self.driver).tap(x=360, y=1120).perform()
        return SessionInfoPage(self.driver)