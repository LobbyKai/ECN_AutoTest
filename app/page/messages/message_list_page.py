#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/27 19:22
# @Author : fengkai
# @File : message_list_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.base.base_page import BasePage


class MessageListPage(BasePage):
    name_element = (MobileBy.XPATH, "//*[@text='万峰']")
    message_element = (MobileBy.XPATH, "//*[@text='万峰']/../..//*[@resource-id='com.yzn.doctor_hepler:id/tv_message']")

    def click_name(self):
        from app.page.messages.session_info_page import SessionInfoPage
        # click [姓名]
        self.find_and_click(*self.name_element)
        return SessionInfoPage(self.driver)

    def get_message(self):
        # 获取患者的咨询状态
        result = self.get_text(*self.message_element)
        return result


