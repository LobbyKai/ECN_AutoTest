#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 10:40
# @Author : fengkai
# @File : record_details_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.base.base_page import BasePage
from app.page.medical_records.instrument.instrument_main_page import InstrumentPage
from app.page.medical_records.medicament.case_info_page import CaseInfoPage
from app.page.workbench.patients_manager.seldoctor_list_page import DoctorListPage


class RecordDetailesPage(BasePage):
    patient_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/createPrescription' and @text='开处方']")
    contract_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/ll_qy']")
    instrument_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/createUtil' and @text='开器械']")
    def click_prescribe(self):
        # click [开处方]
        self.find_and_click(*self.patient_element)
        # 进入开具处方界面
        return CaseInfoPage(self.driver)

    def click_contract(self):
        # click [签约]
        self.find_and_click(*self.patient_element)
        # 进入选择医生团队成员界面
        return DoctorListPage(self.driver)

    def click_instrument(self):
        # click [开器械]
        self.find_and_click(*self.instrument_element)
        # 进入开具器械界面
        return InstrumentPage(self.driver)

    def get_result(self):
        # 获取结果
        result = self.getattr(MobileBy.XPATH, "//*[@class='android.widget.Toast']", "text")
        return result

