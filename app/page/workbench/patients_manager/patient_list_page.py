#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 10:33
# @Author : fengkai
# @File : patient_list_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.base.base_page import BasePage
from app.page.workbench.patients_manager.record_details_page import RecordDetailesPage


class PatientListPage(BasePage):
    patient_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/tv_nickname' and @text='万峰']")

    def click_patient(self):
        # click [患者]
        self.find_and_click(*self.patient_element)
        # 进入病历详情界面
        return RecordDetailesPage(self.driver)