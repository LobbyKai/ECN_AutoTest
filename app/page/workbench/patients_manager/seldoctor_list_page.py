#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 11:05
# @Author : fengkai
# @File : seldoctor_list_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.base.base_page import BasePage
from app.page.workbench.patients_manager.server_package_page import ServerPackgePage


class DoctorListPage(BasePage):
    step_element = (MobileBy.XPATH, "//*[@text='下一步']")

    def click_nextstep(self):
        # click [下一步]
        self.find_and_click(*self.step_element)
        # 进入选择服务包界面
        return ServerPackgePage(self.driver)