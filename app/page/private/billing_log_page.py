#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 15:29
# @Author : fengkai
# @File : billing_log_page.py

from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class BillLogPage(BasePage):
    createtime_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/createTime']")
    back_element = (MobileBy.XPATH, "//*[@resource='com.yzn.doctor_hepler:id/qmui_topbar_item_left_back']")

    def get_first_time(self):
        # get [最新记录创建时间]
        res = self.get_text(*self.createtime_element)
        r = res.split("：")[1]
        r.replace("-", "-")
        return r

    def click_back(self):
        # click [返回]
        from app.page.private.main_page import PrivateInfoPage
        self.find_and_click(*self.back_element)
        return PrivateInfoPage(self.driver)