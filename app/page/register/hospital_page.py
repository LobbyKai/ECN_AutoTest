#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 16:21
# @Author : fengkai
# @File : hospital_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.page.register.hospital_detail_page import HospitalDetailPage


class HospitalPage(BasePage):
    select_element = (MobileBy.XPATH, "//*[@text='东城区']")

    def click_select(self):
        # click [省市区]
        self.find_and_click(*self.select_element)
        return HospitalDetailPage(self.driver)