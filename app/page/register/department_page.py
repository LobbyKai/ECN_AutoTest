#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 16:27
# @Author : fengkai
# @File : department_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class DepartmentPage(BasePage):
    select_element = (MobileBy.XPATH, "//*[@text='心血管内科']")

    def select_department(self):
        # click [科室]
        self.find_and_click(*self.select_element)
        from app.page.register.certification_page import CertificationInfoPage
        return CertificationInfoPage(self.driver)