#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 21:02
# @Author : fengkai
# @File : photo_booth_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class PhotoBoothPage(BasePage):
    select_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/check_view']")
    apply_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/button_apply']")

    def click_select(self):
        # click [勾选]
        self.find_and_click(*self.select_element)
        return self

    def click_apply(self):
        self.click_select()
        # click [使用]
        self.find_and_click(*self.apply_element)
        from app.page.register.certification_page import CertificationInfoPage
        return CertificationInfoPage(self.driver)

    def click_apply_upload(self):
        self.click_select()
        # click [使用]
        self.find_and_click(*self.apply_element)
        from app.page.register.upload_card_page import UploadCardPage
        return UploadCardPage(self.driver)




