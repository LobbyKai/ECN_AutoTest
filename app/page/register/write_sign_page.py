#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 21:11
# @Author : fengkai
# @File : write_sign_page.py
from appium.webdriver.common.touch_action import TouchAction

from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.page.register.continue_page import ContinuePage


class WriteSignPage(BasePage):
    confirm_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/next' and @text='提交认证']")

    def input_sign(self):
        # 输入 [签名]
        TouchAction(self.driver).press(x=100, y=700).wait(1000).move_to(x=180, y=800).\
            wait(1000).move_to(x=610, y=570).release().perform()
        return self

    def click_confirm(self):
        self.input_sign()
        # click [提交认证]
        self.find_and_click(*self.confirm_element)
        return ContinuePage(self.driver)