#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 10:51
# @Author : fengkai
# @File : continue_page.py
from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class ContinuePage(BasePage):
    continue_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/confirm' and @text='继续体验']")

    def click_continue(self):
        # click [继续体验]
        self.find_and_click(*self.continue_element)
        from app.page.main_page import MainPage
        return MainPage(self.driver)