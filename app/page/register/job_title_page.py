#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 15:58
# @Author : fengkai
# @File : job_title_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.base.base_page import BasePage

class JobTitlePage(BasePage):
    job_element = (MobileBy.XPATH,"//*[@resource-id='com.yzn.doctor_hepler:id/name' and @text='住院医师']")

    def click_title(self):
        # click [住院医师]
        self.find_and_click(*self.job_element)
        from app.page.register.certification_page import CertificationInfoPage
        return CertificationInfoPage(self.driver)