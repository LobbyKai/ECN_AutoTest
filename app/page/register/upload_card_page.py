#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 16:51
# @Author : fengkai
# @File : upload_card_page.py
from time import sleep

from app.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy

from app.page.register.department_page import DepartmentPage
from app.page.register.hospital_page import HospitalPage
from app.page.register.job_title_page import JobTitlePage
from app.page.register.photo_booth_page import PhotoBoothPage
from app.page.register.write_sign_page import WriteSignPage


class UploadCardPage(BasePage):
    image_front_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/choosePositive']")
    image_back_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/chooseEmblem']")
    image_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/add_1']")
    card_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/add_2']")
    next_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/next']")
    hospital_element = (MobileBy.XPATH, "//*[@text='请输入或搜索医院名称']/../*[3]")
    department_element = (MobileBy.XPATH, "//*[@text='请输入或者选择科室']/../*[3]")
    upload_element = (MobileBy.XPATH, "//*[@text='上传证件']")

    def upload_front(self):
        # click [人像照]
        self.find_and_click(*self.image_front_element)
        from app.page.register.photo_booth_page import PhotoBoothPage
        return PhotoBoothPage(self.driver)

    def upload_back(self):
        # click [国徽照]
        self.find_and_click(*self.image_back_element)
        return PhotoBoothPage(self.driver)

    def click_add(self):
        # click [添加营业资格证照片]
        self.find_and_click(*self.image_element)
        return PhotoBoothPage(self.driver)

    def swipe_and_click_job(self):
        self.swipe_find("请选择").click()
        sleep(2)
        return JobTitlePage(self.driver)

    def click_add_card(self):
        # click [添加工作证照片]
        self.find_and_click(*self.card_element)
        from app.page.register.photo_booth_page import PhotoBoothPage
        return PhotoBoothPage(self.driver)

    def click_hospital(self):
        # click [医院选择]
        self.find_and_click(*self.hospital_element)
        return HospitalPage(self.driver)

    def click_department(self):
        # click [科室选择]
        self.find_and_click(*self.department_element)
        return DepartmentPage(self.driver)

    def click_next(self):
        # click [下一步]
        self.find_and_click(*self.next_element)
        sleep(2)
        return WriteSignPage(self.driver)