#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 9:56
# @Author : fengkai
# @File : generate_info.py
from faker import Faker


# mock一些测试数据
class GenerateInfo:

    @classmethod
    def get_name(cls):
        # 生成姓名
        return Faker("zh_CN").name()

    @classmethod
    def get_phonenum(cls):
        # 生成电话号
        return Faker("zh_CN").phone_number()