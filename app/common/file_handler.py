#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time : 2022/4/24 15:20
# @Author : fengkai
# @File : file_handler.py
import json
import os
import jsonpath
import yaml
import pymysql


class FileTool():
    @classmethod
    def get_object_dir(cls):
        # 获取api_object的文件夹路径
        return os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    @classmethod
    def read_yaml(cls, file_name):
        # 获取api_object的文件夹路径
        _path = cls.get_object_dir()
        # 拼接yaml文件所在的绝对路径 sep 相当于 win的 \ linux的/ sep.join 需要的参数是一个列表
        yaml_file = os.sep.join([_path, "config", file_name + ".yaml"])
        # 打开 yaml文件 并使用yaml.safe_load 将内容返回出去
        with open(yaml_file, encoding="utf-8") as f:
            return yaml.safe_load(f)

    @classmethod
    def read_json(cls, file_name):
        # 获取api_object的文件夹路径
        path = cls.get_object_dir()
        # 拼接json文件所在的绝对路径 sep 相当于 win的 \ linux的/ sep.join 需要的参数是一个列表
        json_file = os.sep.join([path, "config", file_name + ".json"])
        # 打开 yaml文件 并使用yaml.safe_load 将内容返回出去
        with open(json_file, encoding="utf-8") as f:
            return json.load(f)

    @classmethod
    def get_data_path(cls):
        path = os.sep.join([os.path.dirname(os.path.abspath(__file__)), "../data"])
        return path

    @classmethod
    def get_log_path(cls):
        path = os.sep.join([os.path.dirname(os.path.abspath(__file__)), "../log"])
        print(path)
        return path

    @classmethod
    def jsonpath_util(cls, obj, expr):
        return jsonpath.jsonpath(obj, expr)

    @classmethod
    #连接数据库
    def get_mysql(self, sqlStatement, number):
        #获取数据库中信息
        config = FileTool.read_json("config")["mysqlInfo"]["envInfo"]
        #创建连接
        conn = pymysql.connect(**config)
        #创建游标
        cursor = conn.cursor()
        #执行sql
        cursor.execute(sqlStatement)
        res = cursor.fetchall()
        rowarray_list = []
        #获取sql对应的value值
        for row in res:
            t = (row[number])
            rowarray_list.append(t)
            orderId = rowarray_list[0]
            return orderId
        #断开sql连接
        cursor.close()