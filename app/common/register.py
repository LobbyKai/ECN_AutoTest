#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 11:37
# @Author : fengkai
# @File : register.py
from common.db_handler import DBTool
from common.file_handler import FileTool

class Common:
    def __init__(self):
        self.config_path = FileTool.read_json("config")

    def UpdatePhoneNum(self):
        # 获取更新的sql
        old_phone = self.config_path["patientInfo"]["phoneNum"]
        new_phone = old_phone + "-1"
        # 获取并更新sql语句
        sql1 = self.config_path["mysqlInfo"]["app"]["updatephone"]
        sql2 = sql1.replace("old", old_phone)
        sql = sql2.replace("new", new_phone)
        # 连接数据库查找子订单编号
        data = DBTool().update(sql)
        return data

if __name__ == '__main__':
    d = Common()
    d.UpdatePhoneNum()




