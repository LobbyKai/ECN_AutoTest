#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 11:12
# @Author : fengkai
# @File : server_package_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage


class ServerPackgePage(BasePage):
    package_element = (MobileBy.XPATH, "//*[@text='测试服务包']/../*[@resource-id='com.yzn.doctor_hepler:id/checkbox']")
    send_element = (MobileBy.XPATH, "//*[@text='发送']")

    def click_nextstep(self):
        # click [测试服务包]
        self.find_and_click(*self.package_element)
        # 进入选择服务包界面
        return self

    def click_send(self):
        # 选择 [测试服务包]
        self.click_nextstep()
        # click [发送]
        self.find_and_click(*self.send_element)
        # 进入选择服务包界面
        return ServerPackgePage(self.driver)