#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 14:42
# @Author : fengkai
# @File : instrument_main_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.ios.page.private.billing_log_page import BillLogPage


class PrivateInfoPage(BasePage):
    billing_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '开单记录'")
    setting_element = (MobileBy.IOS_PREDICATE, "name == 'myNewsSettingIcon'")

    def click_usagetype(self):
        # click [开单记录]
        self.find_and_click(*self.billing_element)
        return BillLogPage(self.driver)

    def click_setting(self):
        from app.ios.page.private.setting_page import SettingPage
        # click [设置]
        self.find_and_click(*self.setting_element)
        return SettingPage(self.driver)