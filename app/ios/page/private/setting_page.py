#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 21:27
# @Author : fengkai
# @File : setting_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.page.private.billing_log_page import BillLogPage


class SettingPage(BasePage):
    loginout_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '退出登录'")
    confirm_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '确定'")

    def click_loginout(self):
        # click [退出登录]
        self.find_and_click(*self.loginout_element)
        return self

    def click_confirm(self):
        from app.ios.page.login_page import LoginPage
        self.click_loginout()
        # click [确定]
        self.find_and_click(*self.confirm_element)
        return LoginPage(self.driver)