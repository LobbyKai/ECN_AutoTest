#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 9:54
# @Author : fengkai
# @File : instrument_main_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage
from app.ios.page.messages.message_list_page import MessageListPage
from app.ios.page.messages.consult_info_page import ConsultInfoPage
from app.ios.page.private.main_page import PrivateInfoPage
from app.ios.page.workbench.patients_manager.patient_list_page import PatientListPage


# 首页
class MainPage(BasePage):
    patientlist_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '患者管理'")
    admissions_element = (MobileBy.IOS_PREDICATE, "value == '去查看'")
    refusal_element = (MobileBy.IOS_PREDICATE, "value == '知道了'")
    message_element = (MobileBy.IOS_PREDICATE, "name == '消息'")
    name_new_element = (MobileBy.XPATH, "//*[@type='XCUIElementTypeStaticText' and @name= '我的粉丝']/"
                                        "preceding-sibling::XCUIElementTypeStaticText[2]")
    name_element = (MobileBy.XPATH, "//*[@type='XCUIElementTypeButton' and "
                                              "@name= '接诊中']/../XCUIElementTypeStaticText[6]")
    status_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/authStatus']")
    private_element = (MobileBy.IOS_PREDICATE, "name == '我的'")
    other_element = (MobileBy.XPATH, "//*[@value='万峰']/following-sibling::XCUIElementTypeButton/*[@value='去处理']")

    def goto_mgtpatient(self):
        # click [患者管理]
        self.find_and_click(*self.patientlist_element)
        # 进入患者管理界面
        return PatientListPage(self.driver)

    def goto_consultmenu(self):
        # 判断是否存在【查看】元素
        if self.find(*self.admissions_element):
            # click [查看]
            self.find_and_click(*self.admissions_element)
            return ConsultInfoPage(self.driver)
        else:
            self.goto_consultmenu1()

    def goto_consultmenu1(self):
        # 判断是否存在【去处理】元素
        if self.find(*self.other_element):
            # click [去处理]
            self.find_and_click(*self.other_element)
            return ConsultInfoPage(self.driver)

    def goto_homepage(self):
        # click [知道了]
        self.find_and_click(*self.refusal_element)
        # 不查看接诊界面，返回首页
        return self

    def goto_message_page(self):
        # click [消息]
        self.find_and_click(*self.message_element)
        # 不查看接诊界面，返回首页
        return MessageListPage(self.driver)

    def get_doctor_name_new(self):
        # click [医生姓名]
        result = self.get_text(*self.name_new_element)
        return result

    def get_doctor_name(self):
        # click [医生姓名]
        result = self.get_text(*self.name_element)
        return result

    def get_status(self):
        # click [状态]
        result = self.get_text(*self.status_element)
        return result

    def goto_private_page(self):
        # click [我的]
        self.find_and_click(*self.private_element)
        # 不查看接诊界面，返回首页
        return PrivateInfoPage(self.driver)
