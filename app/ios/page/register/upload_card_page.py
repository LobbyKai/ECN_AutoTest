#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 16:51
# @Author : fengkai
# @File : upload_card_page.py
from time import sleep

from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy

from app.ios.page.register.department_page import DepartmentPage
from app.ios.page.register.hospital_page import HospitalPage
from app.ios.page.register.job_title_page import JobTitlePage
from app.ios.page.register.photo_booth_page import PhotoBoothPage
from app.ios.page.register.write_sign_page import WriteSignPage


class UploadCardPage(BasePage):
    image_front_element = (MobileBy.XPATH, "//*[@value='身份证人像照']/../XCUIElementTypeButton")
    image_back_element = (MobileBy.XPATH, "//*[@value='身份证国徽照']/../XCUIElementTypeButton")
    image_element = (MobileBy.XPATH, "//*[contains(@name,'资质证件')]/../XCUIElementTypeOther[5]")
    card_element = (MobileBy.XPATH, "//*[contains(@name,'工作证')]/../XCUIElementTypeOther[6]")
    next_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '下一步（手写签名）'")
    hospital_element = (MobileBy.XPATH, "//*[@value='医院']/../XCUIElementTypeButton")
    department_element = (MobileBy.XPATH, "//*[@value='科室']/../XCUIElementTypeButton")

    def upload_front(self):
        # click [人像照]
        self.find_and_click(*self.image_front_element)
        return PhotoBoothPage(self.driver)

    def upload_back(self):
        # click [国徽照]
        self.find_and_click(*self.image_back_element)
        return PhotoBoothPage(self.driver)

    def click_add(self):
        # click [添加营业资格证照片]
        self.find_and_click(*self.image_element)
        return PhotoBoothPage(self.driver)

    def swipe_and_click_job(self):
        self.swipe_find("请选择").click()
        sleep(2)
        return JobTitlePage(self.driver)

    def click_add_card(self):
        # click [添加工作证照片]
        self.find_and_click(*self.card_element)
        from app.page.register.photo_booth_page import PhotoBoothPage
        return PhotoBoothPage(self.driver)

    def click_hospital(self):
        # click [医院选择]
        self.find_and_click(*self.hospital_element)
        return HospitalPage(self.driver)

    def click_department(self):
        # click [科室选择]
        self.find_and_click(*self.department_element)
        return DepartmentPage(self.driver)

    def click_next(self):
        # click [下一步]
        self.find_and_click(*self.next_element)
        sleep(2)
        return WriteSignPage(self.driver)