#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 20:42
# @Author : fengkai
# @File : register_main_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.ios.page.register.certification_page import CertificationInfoPage


class RegisterPage(BasePage):
    upload_element = (MobileBy.XPATH, "//*[@type='XCUIElementTypeButton' and @name='上传资料']")
    cancel_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '继续体验'")

    def click_upload(self):
        # click [上传资料]
        self.find_and_click(*self.upload_element)
        return CertificationInfoPage(self.driver)

    def click_cancel(self):
        from app.page.private.setting_page import SettingPage
        # click [继续体验]
        self.find_and_click(*self.cancel_element)
        return self