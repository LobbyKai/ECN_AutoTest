#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 16:24
# @Author : fengkai
# @File : hospital_detail_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class HospitalDetailPage(BasePage):
    select_element = (MobileBy.IOS_PREDICATE, "value == '龙潭社区卫生服务中心'")

    def select_hospital(self):
        # click [医院]
        self.find_and_click(*self.select_element)
        from app.ios.page.register.certification_page import CertificationInfoPage
        return CertificationInfoPage(self.driver)