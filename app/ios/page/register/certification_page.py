#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 20:49
# @Author : fengkai
# @File : certification_page.py
from time import sleep

from appium.webdriver.common.touch_action import TouchAction

from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy

from app.ios.page.register.department_page import DepartmentPage
from app.ios.page.register.hospital_page import HospitalPage
from app.ios.page.register.job_title_page import JobTitlePage
from app.ios.page.register.upload_card_page import UploadCardPage
from app.ios.page.register.write_sign_page import WriteSignPage

class CertificationInfoPage(BasePage):
    name_element = (MobileBy.IOS_PREDICATE , "value == '请输入姓名'", "张清琴")
    id_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeTextField' AND value == '请输入身份证号码'",
                  "410603197308140529")
    image_element = (MobileBy.XPATH, "//*[contains(@name,'资质证件')]/../XCUIElementTypeOther[5]")
    card_element = (MobileBy.XPATH, "//*[contains(@name,'工作证')]/../XCUIElementTypeOther[6]")
    next_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '下一步（手写签名）'")
    hospital_element = (MobileBy.XPATH, "//*[@value='医院']/../XCUIElementTypeButton")
    department_element = (MobileBy.XPATH, "//*[@value='科室']/../XCUIElementTypeButton")
    upload_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '上传证件'")

    def click_upload_card(self):
        # click [上传证件]
        self.find_and_click(*self.upload_element)
        return UploadCardPage(self.driver)

    def  input_name(self):
        # 输入 [姓名]
        self.find_and_send(*self.name_element)
        return self

    def input_id(self):
        # 输入 [证件号码]
        self.find_and_send(*self.id_element)
        return self

    def click_add(self):
        self.input_name()
        self.input_id()
        # click [添加营业资格证照片]
        # TouchAction(self.driver).tap(x=50, y=500).perform()
        # sleep(2)
        self.find_and_click(*self.image_element)
        from app.ios.page.register.photo_booth_page import PhotoBoothPage
        return PhotoBoothPage(self.driver)

    def swipe_and_click_job(self):
        self.swipe_find("请选择").click()
        sleep(2)
        return JobTitlePage(self.driver)

    def click_add_card(self):
        # click [添加工作证照片]
        self.find_and_click(*self.card_element)
        from app.ios.page.register.photo_booth_page import PhotoBoothPage
        return PhotoBoothPage(self.driver)

    def click_hospital(self):
        # click [医院选择]
        self.find_and_click(*self.hospital_element)
        return HospitalPage(self.driver)

    def click_department(self):
        # click [科室选择]
        self.find_and_click(*self.department_element)
        return DepartmentPage(self.driver)

    def click_next(self):
        # click [下一步]
        self.find_and_click(*self.next_element)
        sleep(2)
        return WriteSignPage(self.driver)