#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 21:02
# @Author : fengkai
# @File : photo_booth_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class PhotoBoothPage(BasePage):
    select_element = (MobileBy.XPATH, "//*[@name='Recents']/../XCUIElementTypeOther/XCUIElementTypeOther/"
                                      "XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]")
    apply_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '完成'")

    def click_select(self):
        # click [勾选]
        self.find_and_click(*self.select_element)
        return self

    def click_apply(self):
        self.click_select()
        # click [使用]
        self.find_and_click(*self.apply_element)
        from app.ios.page.register.certification_page import CertificationInfoPage
        return CertificationInfoPage(self.driver)

    def click_apply_upload(self):
        self.click_select()
        # click [使用]
        self.find_and_click(*self.apply_element)
        from app.ios.page.register.upload_card_page import UploadCardPage
        return UploadCardPage(self.driver)




