#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 13:45
# @Author : fengkai
# @File : phone_accept_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage


class PhoneAcceptPage(BasePage):
    phone_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '拨号呼叫'")
    type_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/type']")

    def click_phone(self):
        # click [拨号呼叫]
        self.find_and_click(*self.phone_element)
        return self

    def get_consult_type(self):
        self.click_phone()
        # get [咨询类型字段]
        result = self.get_text(*self.type_element)
        return result

    def test(self):
        self.click_phone()
        res = self.driver.switch_to.alert.accept()
        print("+++++")
        print(res)