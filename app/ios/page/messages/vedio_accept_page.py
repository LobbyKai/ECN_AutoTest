#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/31 9:23
# @Author : fengkai
# @File : vedio_accept_page.py
import time

from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage
from app.ios.page.messages.vedio_chat_page import VedioChatPage


class VedioAcceptPage(BasePage):
    vedio_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '视频通话'")
    back_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == 'back'")

    def click_vedio(self):
        # click [视频通话]
        self.find_and_click(*self.vedio_element)
        time.sleep(2)
        return VedioChatPage(self.driver)

    def click_back(self):
        from app.ios.page.main_page import MainPage
        # click [返回]
        self.find_and_click(*self.back_element)
        return MainPage(self.driver)
