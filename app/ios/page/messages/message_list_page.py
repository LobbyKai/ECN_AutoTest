#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/27 19:22
# @Author : fengkai
# @File : message_list_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage


class MessageListPage(BasePage):
    name_element = (MobileBy.IOS_PREDICATE, "name == '万峰'")
    consult_message_element = (MobileBy.XPATH, "//*[@name='万峰']/following-sibling::XCUIElementTypeStaticText[2]")
    message_element = (MobileBy.XPATH, "//*[@name='万峰']/following-sibling::XCUIElementTypeStaticText[2]")
    vedio_message_element = (MobileBy.XPATH, "//*[@name='万峰']/following-sibling::XCUIElementTypeStaticText[1]")

    def click_name(self):
        from app.ios.page.messages.session_info_page import SessionInfoPage
        # click [姓名]
        self.find_and_click(*self.name_element)
        return SessionInfoPage(self.driver)

    def get_consult_message(self):
        # 获取患者的咨询状态
        result = self.get_text(*self.consult_message_element)
        return result

    def get_message(self):
        # 获取患者的咨询状态
        result = self.get_text(*self.message_element)
        return result

    def get_vedio_message(self):
        # 获取患者的咨询状态
        result = self.get_text(*self.vedio_message_element)
        return result


