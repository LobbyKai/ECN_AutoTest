#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 15:58
# @Author : fengkai
# @File : consult_info_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy

from app.ios.page.messages.phone_accept_page import PhoneAcceptPage
from app.ios.page.messages.session_info_page import SessionInfoPage
from app.ios.page.messages.vedio_accept_page import VedioAcceptPage


class ConsultInfoPage(BasePage):
    confirm_element = (MobileBy.IOS_PREDICATE, "value == '确认接诊'")
    descript_element = (MobileBy.XPATH, "//*[@source-id='com.yzn.doctor_hepler:id/chief']")

    def get_description(self):
        # find [主诉信息]
        result = self.get_text(*self.descript_element)
        return result

    def click_confirm(self):
        # click [确认接诊]
        self.find_and_click(*self.confirm_element)
        return SessionInfoPage(self.driver)

    def click_confirm_vedio(self):
        # click [确认接诊]
        self.find_and_click(*self.confirm_element)
        return VedioAcceptPage(self.driver)

    def click_confirm_phone(self):
        # click [确认接诊]
        self.find_and_click(*self.confirm_element)
        return PhoneAcceptPage(self.driver)