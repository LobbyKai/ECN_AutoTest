#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 19:13
# @Author : fengkai
# @File : instrument_main_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage
from app.ios.page.medical_records.instrument.instrument_list_page import InstrumentListPage


class InstrumentPage(BasePage):
    add_element = (MobileBy.IOS_PREDICATE, "name == 'newPrescribing Add'")
    submit_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '提交'")

    def click_add(self):
        # click [添加按钮]
        self.find_and_click(*self.add_element)
        # 进入添加器械界面
        return InstrumentListPage(self.driver)

    def click_submit(self):
        from app.ios.page.workbench.patients_manager.record_details_page import RecordDetailesPage
        # click [添加按钮]
        self.find_and_click(*self.submit_element)
        # 进入添加器械界面
        return RecordDetailesPage(self.driver)