#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 19:35
# @Author : fengkai
# @File : confirm_goods_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy



class OpenInstrumentPage(BasePage):
    confirm_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '确定'")

    def click_confirm(self):
        from app.ios.page.medical_records.instrument.instrument_main_page import InstrumentPage
        # click [确定]
        self.find_and_click(*self.confirm_element)
        # 进入选择服务包界面
        return InstrumentPage(self.driver)
