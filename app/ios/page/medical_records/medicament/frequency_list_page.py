#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 19:53
# @Author : fengkai
# @File : frequency_list_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class FrequencyListPage(BasePage):
    frequency_element = (MobileBy.IOS_PREDICATE, "name == '每早一次'")

    def click_usagetype(self):
        from app.ios.page.medical_records.medicament.drug_edit_page import EditDrugPage
        # click [每早一次]
        self.find_and_click(*self.frequency_element)
        return EditDrugPage(self.driver)