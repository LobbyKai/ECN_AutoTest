#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/27 14:04
# @Author : fengkai
# @File : taboo_list_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage


class TabooListPage(BasePage):
    taboo_element = (MobileBy.IOS_PREDICATE, "name == '请输入诊断禁忌...'", "Auto_test")
    save_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '保存'")
    finish_element = (MobileBy.XPATH, "//*[@name='Title']/../XCUIElementTypeButton")

    def input_taboo(self):
        # 输入禁忌信息
        self.find_and_send(*self.taboo_element)
        return self

    def keyboard_done(self):
        # click [完成]
        self.find_and_click(*self.finish_element)
        return self

    def click_confirm(self):
        # click [保存]
        from app.ios.page.medical_records.medicament.case_info_page import CaseInfoPage
        self.input_taboo()
        self.keyboard_done()
        # 点击[保存]
        self.find_and_click(*self.save_element)
        return CaseInfoPage(self.driver)

