#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/27 13:57
# @Author : fengkai
# @File : allergy_list_page.py
from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage


class AllergyListPage(BasePage):
    allergy_element = (MobileBy.XPATH, "//*[@type='XCUIElementTypeButton' and @name='确认']/../"
                                       "XCUIElementTypeCell/XCUIElementTypeButton[3]")
    save_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '确认'")

    def click_penicillin(self):
        # click [青霉素类]
        self.find_and_click(*self.allergy_element)
        return self

    def click_save(self):
        # click [确认]
        from app.ios.page.medical_records.medicament.case_info_page import CaseInfoPage
        self.click_penicillin()
        self.find_and_click(*self.save_element)
        return CaseInfoPage(self.driver)
