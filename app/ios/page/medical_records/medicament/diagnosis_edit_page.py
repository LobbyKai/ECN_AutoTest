#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 10:12
# @Author : fengkai
# @File : diagnosis_edit_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy


class DiagnosisEditPage(BasePage):
    diagnosis_element= (MobileBy.IOS_PREDICATE, "name == '请输入临床诊断...'", "Auto_test")
    confirm_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '保存'")
    finish_element = (MobileBy.XPATH, "//*[@name='Title']/../XCUIElementTypeButton")

    def send_info(self):
        # click [诊断信息]
        self.find_and_send(*self.diagnosis_element)
        return self

    def keyboard_done(self):
        # click [完成]
        self.find_and_click(*self.finish_element)
        return self

    def click_confirm(self):
        from app.ios.page.medical_records.medicament.case_info_page import CaseInfoPage
        # 输入信息
        self.send_info()
        self.keyboard_done()
        # 点击[保存]
        self.find_and_click(*self.confirm_element)
        return CaseInfoPage(self.driver)
