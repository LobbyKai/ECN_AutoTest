#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 18:09
# @Author : fengkai
# @File : drug_edit_page.py
from app.ios.base.base_page import BasePage
from appium.webdriver.common.mobileby import MobileBy
from app.ios.page.medical_records.medicament.frequency_list_page import FrequencyListPage
from app.ios.page.medical_records.medicament.usage_list_page import UsageListPage


class EditDrugPage(BasePage):
    usercount_element = (MobileBy.XPATH, "//*[@type='XCUIElementTypeButton' and @name='+']/"
                                         "following-sibling::XCUIElementTypeTextField[1]", "1")
    usage_element = (MobileBy.XPATH, "//*[@type='XCUIElementTypeButton' and @name='+']/"
                                     "following-sibling::XCUIElementTypeButton[1]")
    frequency_element =(MobileBy.XPATH, "//*[@name='用药频率：']/preceding-sibling:: XCUIElementTypeButton[1]")
    confirm_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '确定'")

    def input_usercount(self):
        # 输入用量
        self.find_and_send(*self.usercount_element)
        return self

    def click_usage(self):
        # 用量
        self.input_usercount()
        # 点击用法
        self.find_and_click(*self.usage_element)
        return UsageListPage(self.driver)

    def click_frequency(self):
        # 点击用药频率
        self.find_and_click(*self.frequency_element)
        return FrequencyListPage(self.driver)

    def click_confirm(self):
        from app.ios.page.medical_records.medicament.case_info_page import CaseInfoPage
        # 点击确定
        self.find_and_click(*self.confirm_element)
        return CaseInfoPage(self.driver)

