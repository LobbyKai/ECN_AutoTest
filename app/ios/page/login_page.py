#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 13:44
# @Author : fengkai
# @File : login_page.py
from time import sleep

from appium.webdriver.common.mobileby import MobileBy
from app.ios.base.base_page import BasePage
from app.ios.page.main_page import MainPage
from app.ios.page.register.certification_page import CertificationInfoPage
from app.ios.page.register.register_main_page import RegisterPage


class LoginPage(BasePage):
    phone_element = (MobileBy.IOS_PREDICATE, "//*[@text='+86']")
    clear_phone_element = (MobileBy.IOS_PREDICATE, "name == '清除文本'")
    click_phone_element = (MobileBy.IOS_PREDICATE, "value == '15991698205'")
    click_phone_element1 = (MobileBy.IOS_PREDICATE, "value == '17792989619'")
    edit_phone_element =  (MobileBy.IOS_PREDICATE, "value == '请输入手机号码'", "17792989619")
    passwd_element = (MobileBy.IOS_PREDICATE, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']")
    clear_passwd_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']")
    edit_passwd_element =  (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeSecureTextField' AND "
                                                    "value == '请输入密码'", "989619")
    login_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeButton' AND name == '登录'")
    agree_element = (MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/login' and @text='登录'] ")
    verify_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeStaticText' AND name == '验证码登录'")
    code_element = (MobileBy.IOS_PREDICATE, "value == '请输入验证码'", "123456")
    new_phone_element =  (MobileBy.IOS_PREDICATE, "value == '请输入手机号码'", "15991698205")
    upload_element = (MobileBy.IOS_PREDICATE, "type == 'XCUIElementTypeStaticText' AND name == '上传资料'")


    def clear_phone(self):
        # clear [电话号码]
        self.find_and_click(*self.clear_phone_element)
        return self

    def click_phone(self):
        # click [电话号码]
        self.find_and_click(*self.click_phone_element)
        return self

    def click_phone_new(self):
        # click [电话号码]
        self.find_and_click(*self.click_phone_element1)
        return self

    def input_phone(self):
        # 输入 [电话号码]
        self.find_and_send(*self.edit_phone_element)
        return self

    def input_phone_new(self):
        # 输入 [电话号码]
        self.find_and_send(*self.new_phone_element)
        return self

    def clear_passwd(self):
        # clear [密码]
        self.find_and_clear(*self.passwd_element)
        return self

    def input_passwd(self):
        # 输入 [密码]
        self.find_and_send(*self.edit_passwd_element)
        return self

    def click_agree(self):
        # click [同意勾选]
        self.find_and_click(*self.agree_element)
        return self

    def click_load(self):
        # click [上传资料]
        self.find_and_click(*self.upload_element)
        return CertificationInfoPage(self.driver)

    def click_login(self):
        self.click_phone_new()
        self.clear_phone()
        self.input_phone()
        self.input_passwd()
        # click [登录]
        self.find_and_click(*self.login_element)
        return MainPage(self.driver)

    def click_verfiy(self):
        # click [验证码登录]
        self.find_and_click(*self.verify_element)
        return self

    def input_code(self):
        # 输入 [验证码]
        self.find_and_send(*self.code_element)
        return self

    def verfiy_login(self):
        self.click_verfiy()
        self.click_phone_new()
        self.clear_phone()
        self.input_phone()
        self.input_code()
        # click [登录]
        self.find_and_click(*self.login_element)
        return MainPage(self.driver)

    def return_login(self):
        self.click_verfiy()
        self.click_phone()
        self.clear_phone()
        self.input_phone()
        self.input_code()
        # click [登录]
        self.find_and_click(*self.login_element)
        return MainPage(self.driver)

    def register_login(self):
        self.click_verfiy()
        self.click_phone_new()
        self.clear_phone()
        self.input_phone_new()
        self.input_code()
        # click [登录]
        self.find_and_click(*self.login_element)
        return RegisterPage(self.driver)