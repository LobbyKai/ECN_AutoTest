#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 9:50
# @Author : fengkai
# @File : base_driver.py
import logging

from appium import webdriver
from app.ios.base.base_page import BasePage
from app.ios.page.login_page import LoginPage
from app.ios.page.main_page import MainPage
from app.ios.common.file_handler import FileTool

class BaseDriver(BasePage):
    def __init__(self):
        BasePage.__init__(self)
        self.json_path = FileTool.read_json("device_info")
        self.yaml_path = FileTool.read_yaml("device_info")
        self.device_type = "mumu"

    def start(self):
        device_info = self.yaml_path.get(self.device_type)
        # 判断配置文件是否存在环境信息
        if device_info:
            if "url" not in device_info or "port" not in device_info:
                assert "配置信息不存在"
        else:
            assert "配置信息不存在"
        logging.info(f"设备信息 ===>>>:{device_info}")
        if self.driver == None:
            logging.info(f"driver 为 {self.driver}")
            # 获取配置信息
            desire_caps = self.json_path["mulator"]["ios"]
            # 启动app
            self.driver = webdriver.Remote(f"http://{device_info['url']}:{device_info['port']}/wd/hub", desire_caps)
            # 隐式等待，每一次查找元素的时候，动态的查找
            self.driver.implicitly_wait(10)

        else:
            logging.info(f"复用 driver. ")
            # 启动app 默认启动desire里的app
            self.driver.launch_app()
            # self.driver.start_activity()
        return self

    def stop(self):
        # 停止 app
        self.driver.quit()

    def restart(self):
        # 重启app
        self.driver.close_app()
        self.driver.launch_app()

    def goto_main(self):
        # 主页入口
        return MainPage(self.driver)

    def goto_login(self):
        # 登录入口
        return LoginPage(self.driver)

if __name__ == '__main__':
    d = BaseDriver()
    d.start()