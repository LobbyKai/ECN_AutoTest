#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 20:14
# @Author : fengkai
# @File : test_smoking_workbench_prescribe_0010.py
import allure
import pytest

from common.file_handler import FileTool
from common.logger_handler import logger
from app.ios.base.base_driver import BaseDriver
from api.wechat.consult import WechatConsult


WechatConsult=WechatConsult("env1")

@allure.feature("图文咨询开药测试")
class TestConsult:
    @allure.story("开具处方药测试")
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        result = self.config_path["Consult"]["responseMsg"]["msg1"]
        # 小程序端发送图文咨询
        self.send_msg = WechatConsult.ConfirmGraphicsOrder()
        assert self.send_msg == result
        logger.info(f"发送图文咨询成功")
        # 启动app
        self.app = BaseDriver()
        # 进入主页
        self.main = self.app.start().goto_main()

    @pytest.mark.parametrize("msg", ["发送成功"])
    def testcase(self, msg):
        # 图文咨询开具处方并发送
        self.main.goto_consultmenu1().click_confirm().click_prescribe().click_diagnosis().click_confirm().\
            click_allergy().click_save().click_taboo().click_confirm().click_drugadd().click_add().click_usage().\
            click_usagetype().click_frequency().click_usagetype().click_confirm().click_submit()
        logger.info(f"向患者开具处方药成功")

    def teardown_class(self):
        # 重启app
        self.app.restart()
        # # 结束咨询
        result = self.main.goto_message_page().click_name().click_confirm().get_message()
        assert result == "图文咨询已结束"
        logger.info(f"咨询会话已关闭")
        # 关闭app
        self.app.stop()
