#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 20:26
# @Author : fengkai
# @File : test_smoking_consult_0012.py
import allure
import pytest

from api.wechat.consult import WechatConsult
from common.file_handler import FileTool
from common.logger_handler import logger
from app.ios.base.base_driver import BaseDriver

WechatConsult=WechatConsult("env1")

@allure.feature("视频咨询测试")
class TestConsult:
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        result = self.config_path["Consult"]["responseMsg"]["msg1"]
        # 小程序端发送视频咨询
        self.send_msg = WechatConsult.ConfirmVideoOrder()
        assert self.send_msg == result
        logger.info(f"发送视频咨询成功")
        # 启动app
        self.app = BaseDriver()
        # 进入主页
        self.main = self.app.start().goto_main()

    @pytest.mark.parametrize("msg", ["视频咨询已结束"])
    def testcase(self, msg):
        # 接受并结束视频通话
        result = self.main.goto_consultmenu().click_confirm_vedio().click_vedio().click_hangup().\
            click_back().goto_message_page().click_name().click_confirm().get_vedio_message()
        # 判断视频通话状态
        assert result == msg
        logger.info(f"视频咨询会话已关闭")

    def teardown_class(self):
        # 关闭app
        self.app.stop()
