#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 20:26
# @Author : fengkai
# @File : test_smoking_consult_0011.py
import allure
import pytest

from api.wechat.consult import WechatConsult
from common.file_handler import FileTool
from common.logger_handler import logger
from app.ios.base.base_driver import BaseDriver

WechatConsult=WechatConsult("env1")

@allure.feature("电话咨询测试")
class TestConsult:
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        result = self.config_path["Consult"]["responseMsg"]["msg1"]
        # 小程序端发送电话咨询
        self.send_msg = WechatConsult.ConfirmPhoneOrder()
        assert self.send_msg == result
        logger.info(f"发送电话咨询成功")
        # 启动app
        self.app = BaseDriver()
        # 进入主页
        self.main = self.app.start().goto_main()

    @pytest.mark.parametrize("msg", ["电话咨询已结束"])
    def testcase(self, msg):
        # 接受并结束电话通话
        self.main.goto_consultmenu().click_confirm_phone().click_phone()
        # 重启app
        self.app.restart()
        # 关闭电话咨询会话
        result = self.main.goto_message_page().click_name().click_confirm().get_message()
        assert result == msg
        logger.info(f"电话咨询会话已关闭")

    def teardown_class(self):
        # 关闭app
        self.app.stop()