#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/23 17:00
# @Author : fengkai
# @File : demo_0010.py
import time

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction

from common.file_handler import FileTool

class Testaaap:
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        desired_caps = {
            "platformName": "ios",
            "platformVerison": "15.1",
            "deviceName": "iPhone 13 Pro Max",
            "app": "/Users/edy/Library/Developer/Xcode/DerivedData/spr_ly-fcututolzceubjgouwjvpbneymjy/Build/Products/Debug-iphonesimulator/spr_ly.app",
            "noReset": "True",
            "automationName": "XCUITest"
        }
        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        self.driver.implicitly_wait(5)
    def testcase(self):
        # # //*[@text='+86']
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/qmui_tab_segment_item_id' "
        #                                "and @text='消息']").click()
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='万峰']").click()
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='视频通话']").click()
        time.sleep(5)
        # TouchAction(self.driver).tap(x=280, y=500).perform()
        re = self.driver.find_element(MobileBy.IOS_PREDICATE, "name == '我的'").click()
        re = self.driver.find_element_by_accessibility_id('我的')
        # re.click()
        time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='com.yzn.doctor_hepler:id/qmui_topbar_item_left_back']").click()
        res = self.driver.find_element(MobileBy.XPATH, "//*[@text='开单时间：2022-05-30']").click()
        # res = self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/createTime']").text
        # print(res)
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/clearPhone']").click()
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPhone']")\
        #     .send_keys("17792989619")
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']") \
        #     .click()
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']") \
        #     .clear()
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editPass']")\
        #     .send_keys("989619")
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/iv_agree_check']").click()
        # # 点击登录
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/login']") \
        #     .click()
        # time.sleep(2)
        # 选择患者管理中的一个患者
        # self.driver.implicitly_wait(5)
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/tab_patient_m']") \
        #     .click()
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/tv_nickname' and @text='万峰']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/createPrescription' and @text='开处方']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/diagnose']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editText']").send_keys("Autotest")
        # # time.sleep(3)
        #
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/confirm']").click()
        #
        #
        #
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/taboo']").click()
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/editText']").send_keys("Autotest")
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/confirm' and @text='保存']").click()
        # time.sleep(3)
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH, "//*[@resource-id='com.yzn.doctor_hepler:id/addMedicineImage']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/edit']").send_keys("咽炎片")
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/clear']").click()
        # # time.sleep(2)
        # # self.driver.find_element(MobileBy.XPATH,
        # #                          "//*[@resource-id='com.yzn.doctor_hepler:id/pharmacyName' and @text='大头儿子']").click()
        #
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@text='大头儿子']/../../..//*[@resource-id='com.yzn.doctor_hepler:id/add']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/medicineUseCount']").send_keys("1")
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/medicineUsage']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/text' and @text='口服']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/medicineFrequency']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/text' and @text='每早一次']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/confirmButton']").click()
        # # time.sleep(2)
        # self.driver.find_element(MobileBy.XPATH,
        #                          "//*[@resource-id='com.yzn.doctor_hepler:id/submitButton']").click()
        # # time.sleep(2)
        # res = self.driver.find_element(MobileBy.XPATH, "//*[@class='android.widget.Toast']").get_attribute("text")
        # print(res)

    def teardown_class(self):
        self.driver.quit()