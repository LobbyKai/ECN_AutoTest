#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 20:25
# @Author : fengkai
# @File : test_smoking_consult_0010.py
import allure
import pytest

from api.wechat.consult import WechatConsult
from common.file_handler import FileTool
from common.logger_handler import logger
from app.base.base_driver import BaseDriver

WechatConsult=WechatConsult("env1")

@allure.feature("图文咨询测试")
class TestConsult:
    def setup_class(self):
        self.config_path = FileTool.read_json("config")
        result = self.config_path["Consult"]["responseMsg"]["msg1"]
        # 小程序端发送图文咨询
        self.send_msg = WechatConsult.ConfirmGraphicsOrder()
        assert self.send_msg == result
        logger.info(f"发送图文咨询成功")
        # 启动app
        self.app = BaseDriver()
        # 进入主页
        self.main = self.app.start().goto_main()

    @pytest.mark.parametrize("msg", ["图文咨询已结束"])
    def testcase(self, msg):
        # 图文咨询开具处方并发送
        result = self.main.goto_consultmenu().click_confirm().click_over().goto_message_page().get_message()
        assert result == msg
        logger.info(f"图文咨询会话已关闭")

    def teardown_class(self):
        # 关闭app
        self.app.stop()
