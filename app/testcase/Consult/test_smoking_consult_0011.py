#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 20:26
# @Author : fengkai
# @File : test_smoking_consult_0011.py
import allure
import pytest

from api.wechat.consult import WechatConsult
from common.logger_handler import logger
from app.base.base_driver import BaseDriver

WechatConsult=WechatConsult("env1")

@allure.feature("电话咨询测试")
class TestConsult:
    def setup_class(self):
        # 启动app
        self.app = BaseDriver()
        # 进入主页
        self.main = self.app.start().goto_main()

    @pytest.mark.run(order=1)
    @pytest.mark.parametrize("msg1", ["0元订单，无需支付！"])
    def testcase1(self, msg1):
        self.send_msg = WechatConsult.ConfirmPhoneOrder()
        assert self.send_msg == msg1
        logger.info(f"发送电话咨询成功")

    @pytest.mark.run(order=2)
    @pytest.mark.parametrize("msg", ["电话咨询已结束"])
    def testcase(self, msg):
        # 接受并结束电话通话
        res = self.main.goto_consultmenu().click_confirm_phone().get_consult_type()
        # # # 判断电话通话状态
        assert res == "电话咨询"
        # # # 重启app
        self.app.restart()
        # 关闭电话咨询会话
        result = self.main.goto_message_page().click_name().click_confirm().get_message()
        assert result == msg
        logger.info(f"电话咨询会话已关闭")

    def teardown_class(self):
        # 关闭app
        self.app.stop()