#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/6/1 15:35
# @Author : fengkai
# @File : test_smoking_register_0020.py
import allure
import pytest

from common.logger_handler import logger
from app.base.base_driver import BaseDriver
from app.common.register import Common


@allure.feature("注册测试")
class TestConsult:
    @allure.story("验证码注册测试")
    def setup_class(self):
        # 启动app
        self.app = BaseDriver()
        # 退出登录
        self.out = self.app.start().goto_main().goto_private_page().click_setting().click_confirm()
        # 重启app
        self.app.restart()
        # 进入主页
        self.main = self.app.start().goto_login()

    @pytest.mark.parametrize("msg", ["15991698205"])
    def testcase(self, msg):
        # 验证码注册
        result = self.main.register_login().click_upload().click_upload_card().upload_front().click_apply_upload().\
            upload_back().click_apply_upload().click_add().click_apply_upload().swipe_and_click_job().\
            click_title().click_add_card().click_apply().click_hospital().click_select().select_hospital().\
            click_department().select_department().click_next().click_confirm().click_continue().get_doctor_name()
        assert result == msg
        logger.info(f"注册信息提交成功")

    def teardown_class(self):
        # 数据库中注销注册手机号
        self.delete = Common().UpdatePhoneNum()
        # 退出新注册账号首页
        self.out = self.app.start().goto_main().goto_private_page().click_setting().click_confirm()
        # # 重启app
        self.app.restart()
        # # 验证码登录首页
        self.name = self.main.verfiy_login().get_doctor_name()
        assert self.name == "冯凯"
        # 关闭app
        self.app.stop()