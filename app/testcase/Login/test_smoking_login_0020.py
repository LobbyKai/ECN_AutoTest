#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 14:06
# @Author : fengkai
# @File : test_smoking_login_0020.py
import allure
import pytest
from common.logger_handler import logger
from app.base.base_driver import BaseDriver

@allure.feature("登录测试")
class TestConsult:
    @allure.story("密码登录测试")
    def setup_class(self):
        # 启动app
        self.app = BaseDriver()
        # 退出登录
        self.out = self.app.start().goto_main().goto_private_page().click_setting().click_confirm()
        # 重启app
        self.app.restart()
        # 进入主页
        self.main = self.app.start().goto_login()

    @pytest.mark.parametrize("msg", ["冯凯"])
    def testcase(self, msg):
        # 密码登录app
        result = self.main.verfiy_login().get_doctor_name()
        assert result == msg
        logger.info(f"登录成功")

    def teardown_class(self):
        # 关闭app
        self.app.stop()
