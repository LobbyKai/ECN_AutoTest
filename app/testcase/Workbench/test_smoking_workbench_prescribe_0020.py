#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/30 14:26
# @Author : fengkai
# @File : test_smoking_workbench_prescribe_0020.py
import allure
import pytest
import datetime
from common.logger_handler import logger
from app.base.base_driver import BaseDriver

@allure.feature("患者管理开药测试")
class TestConsult:
    @allure.story("开具处方药测试")
    def setup_class(self):
        # 启动app
        self.app = BaseDriver()
        # 进入主页1
        self.main = self.app.start().goto_main()

    @pytest.mark.parametrize("msg", ["发送成功"])
    def testcase(self, msg):
        # 患者管理开具处方并发送
        result = self.main.goto_mgtpatient().click_patient().click_prescribe().\
            click_diagnosis().click_confirm().click_allergy().click_save().click_taboo().click_confirm().\
            click_drugadd().click_add().input_usercount().click_usage().click_usagetype().click_frequency().\
            click_usagetype().click_confirm().click_submit().get_result()
        assert result == msg
        # 重启app
        self.app.restart()
        # 获取最新处方记录的开具时间
        result = self.main.goto_private_page().click_usagetype().get_first_time()
        # 获取当天时间，时间格式为YY-MM-DD
        self.time = datetime.date.today()
        # 判断时间是否正确
        assert result == str(self.time)
        logger.info(f"处方记录存在，时间正确")

    def teardown_class(self):
        # 关闭app
        self.app.stop()
