#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time : 2022/5/26 20:14
# @Author : fengkai
# @File : test_smoking_workbench_prescribe_0010.py
import allure
import pytest

from common.file_handler import FileTool
from common.logger_handler import logger
from app.base.base_driver import BaseDriver
from api.wechat.consult import WechatConsult

WechatConsult=WechatConsult("env1")

@allure.feature("图文咨询开药测试")
class TestConsult:
    @allure.story("开具处方药测试")
    def setup_class(self):
        # 启动app
        self.app = BaseDriver()
        # 进入主页
        self.main = self.app.start().goto_main()

    @pytest.mark.run(order=1)
    @pytest.mark.parametrize("msg1", ["0元订单，无需支付！"])
    def testcase1(self, msg1):
        self.send_msg = WechatConsult.ConfirmGraphicsOrder()
        assert self.send_msg == msg1
        logger.info(f"发送图文咨询成功")

    @pytest.mark.run(order=2)
    @pytest.mark.parametrize("msg", ["发送成功"])
    def testcase(self, msg):
        # 图文咨询开具处方并发送
        result = self.main.goto_consultmenu().click_confirm().click_prescribe().\
            click_diagnosis().click_confirm().click_allergy().click_save().click_taboo().click_confirm().\
            click_drugadd().click_add().input_usercount().click_usage().click_usagetype().click_frequency().\
            click_usagetype().click_confirm().click_submit().get_result()
        assert result == msg
        logger.info(f"向患者开具处方药成功")

    def teardown_class(self):
        # 重启app
        self.app.restart()
        # # 结束咨询
        result = self.main.goto_message_page().click_name().click_confirm().get_message()
        assert result == "图文咨询已结束"
        logger.info(f"咨询会话已关闭")
        # 关闭app
        self.app.stop()
